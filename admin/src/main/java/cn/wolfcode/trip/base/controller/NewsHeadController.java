package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.query.NewsQueryObject;
import cn.wolfcode.trip.base.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/newsHead")
public class NewsHeadController {

    @Autowired
    private INewsService newsService;


    // 获得所有发布的日报
    @RequestMapping("/list")
    public Object list(Model model, @ModelAttribute("qo")NewsQueryObject qo){
        // 只查询头条日报状态
        qo.setState(3);
        model.addAttribute("pageInfo",newsService.query(qo));
        return "newsHead/list";
    }
}
