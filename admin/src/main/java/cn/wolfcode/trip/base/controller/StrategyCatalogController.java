package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.query.StrategyCatalogQueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import cn.wolfcode.trip.base.service.IStrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/strategyCatalog")
public class StrategyCatalogController {


    @Autowired
    private IStrategyCatalogService strategyCatalogService;

    @Autowired
    private IStrategyService strategyService;


    // 攻略分类列表
    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") StrategyCatalogQueryObject qo, Model model){
        qo.setPageSize(6);
        model.addAttribute("pageInfo",strategyCatalogService.query(qo));
        model.addAttribute("strategies", strategyService.list());
        return "/strategyCatalog/list";
    }


    // 增添或修改攻略分类
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(StrategyCatalog entity){
        strategyCatalogService.saveOrUpdate(entity);
        return "redirect:/strategyCatalog/list.do";
    }

    // 根据大攻略的id来查询可用的攻略分类
    @RequestMapping("/listByStrategyId")
    @ResponseBody
    public Object listByStrategyId(Long strategyId){

        return strategyCatalogService.list(strategyId);
    }


}
