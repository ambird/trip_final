package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.query.NewsQueryObject;
import cn.wolfcode.trip.base.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/newsRelease")
public class NewsReleaseController {

    @Autowired
    private INewsService newsService;


    // 获得所有发布的日报
    @RequestMapping("/list")
    public Object list(Model model, @ModelAttribute("qo")NewsQueryObject qo){
        // 只查询已发布状态
        qo.setState(2);
        // 按照最近更新时间升序排列
        qo.setOrderBy("releaseTime desc");
        model.addAttribute("pageInfo",newsService.query(qo));
        return "newsRelease/list";
    }
}
