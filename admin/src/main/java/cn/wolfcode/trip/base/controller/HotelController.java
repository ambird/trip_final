package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.Hotel;
import cn.wolfcode.trip.base.service.IHotelService;
import cn.wolfcode.trip.base.service.IRegionService;
import cn.wolfcode.trip.base.util.HotelQueryObject;
import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/hotel")
public class HotelController {

    //注入IHotelService
    @Autowired
    private IHotelService hotelService;

    //将信息要以下拉框的形式表现在模态框当中,编写service
    @Autowired
    private IRegionService regionService;


    //编写映射路径
    @RequestMapping("/list")
    public String getHotelsInAdmin(Model model, @ModelAttribute("qo") HotelQueryObject qo) {

        model.addAttribute("pageInfo", hotelService.queryHotelInAdmin(qo));
        model.addAttribute("regions", regionService.list(null));

        return "hotel/list";
    }

    //保存或者更新
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Hotel entity, MultipartFile img) {

        if (img != null && img.getContentType().startsWith("image") && img.getSize() > 0) {
            String url = UploadUtil.upload(img);
            entity.setCoverUrl(url);
        }

        hotelService.saveOrUpdate(entity);
        return "redirect:/hotel/list.do";
    }

}
