package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/image")
public class ImageController {


    @RequestMapping("/upload")
    public Object upload(@RequestParam MultipartFile upload){
        Map<String, Object> map = new HashMap<>();
        if(upload.getContentType().startsWith("image") && upload.getSize() > 0){
            // 把上传的图片保存到本地图库
            String imgUrl = UploadUtil.upload(upload);
            map.put("uploaded",1);
            map.put("url",imgUrl);
        }

        return map;
    }

}
