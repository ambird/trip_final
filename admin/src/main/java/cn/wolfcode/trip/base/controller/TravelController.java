package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/travel")
public class TravelController {

    @Autowired
    private ITravelService travelService;


    // 获得所有游记
    @RequestMapping("/list")
    public Object list(Model model, @ModelAttribute("qo")TravelQueryObject qo){
        // 如果没有选择状态，默认查询待审核
        if(qo.getState() == null){
            qo.setState(Travel.STATE_WAIT);
        }

        // 必须是可公开的状态
        qo.setIsPublic(true);
        // 按提交时间升序
        qo.setOrderBy("t.lastUpdateTime");

        model.addAttribute("pageInfo",travelService.queryTravel(qo));

        return "/travel/list";
    }

    // 获得单个游记内容（查看内容）
    // 存在一个编码问题，所以要设置一个produces
    @RequestMapping(value = "/getContents", produces = "text/html;charset=UTF-8")
    @ResponseBody
    public Object getContent(Long id){
        return travelService.getContent(id);
    }

    // 更新游记状态（提交/拒绝）
    @RequestMapping("/changeState")
    @ResponseBody
    public Object changeState(Long id, Integer state){
        travelService.changeState(id, state);
        return new JSONResult();
    }
}
