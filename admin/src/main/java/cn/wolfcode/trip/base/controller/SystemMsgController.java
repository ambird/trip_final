package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.SystemMsg;
import cn.wolfcode.trip.base.query.SystemMsgQueryObject;
import cn.wolfcode.trip.base.service.ISystemMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/systemMsg")
public class SystemMsgController {

    @Autowired
    private ISystemMsgService systemMsgService;

    @RequestMapping("/list")
    public String list(Model model,@ModelAttribute("qo") SystemMsgQueryObject qo){
        model.addAttribute("pageInfo",systemMsgService.query(qo));
        return "systemMsg/list";
    }

    @RequestMapping(value = "/getContent",produces = "text/html;charset=utf-8")
    @ResponseBody
    public Object getContent(Long id){
        return systemMsgService.selectContentById(id);
    }

    @RequestMapping("/save")
    public Object save(SystemMsg entity){
        systemMsgService.save(entity);
        return "redirect:/systemMsg/list.do";
    }
}
