package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.HotelOrder;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IHotelOrderService;
import cn.wolfcode.trip.base.service.IHotelService;
import cn.wolfcode.trip.base.service.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hotelOrder")
public class HotelOrderController {


    //注入酒店订单的service
    @Autowired
    private IHotelOrderService hotelOrderService;

    //将酒店名称注入
    @Autowired
    private IHotelService hotelService;

    //注入区域的对象
    @Autowired
    private IRegionService regionService;

    @RequestMapping("/list")
    public String list(Model model, @ModelAttribute("qo") QueryObject qo) {

        model.addAttribute("pageInfo", hotelOrderService.query(qo));
        model.addAttribute("hotels", hotelService.queryHotels());
        model.addAttribute("regions", regionService.list(null));

        return "hotelOrder/list";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(HotelOrder entity) {

        hotelOrderService.saveOrUpdate(entity);

        return "redirect:/hotelOrder/list.do";
    }


}
