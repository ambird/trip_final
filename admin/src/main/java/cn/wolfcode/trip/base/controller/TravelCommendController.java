package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.TravelCommendQueryObject;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/travelCommend")
public class TravelCommendController {

    @Autowired
    private ITravelCommendService travelCommendService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") TravelCommendQueryObject qo, Model model){
        qo.setOrderBy("schedule desc");
        model.addAttribute("pageInfo", travelCommendService.query(qo));

        return "travelCommend/list";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(TravelCommend entity, MultipartFile img){
        // 先处理上传图片问题
        if(img != null && img.getContentType().startsWith("image") && img.getSize() > 0){
            String url = UploadUtil.upload(img);
            entity.setCoverUrl(url);

        }
        travelCommendService.saveOrUpdate(entity);
        return "redirect:/travelCommend/list.do";
    }


}
