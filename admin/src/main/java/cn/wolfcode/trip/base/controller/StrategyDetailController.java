package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import cn.wolfcode.trip.base.service.IStrategyDetailService;
import cn.wolfcode.trip.base.service.IStrategyService;
import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/strategyDetail")
public class StrategyDetailController {


    @Autowired
    private IStrategyDetailService strategyDetailService;

    @Autowired
    private IStrategyCatalogService strategyCatalogService;

    @Autowired
    private IStrategyService strategyService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo")QueryObject qo, Model model){
        qo.setPageSize(4);
        qo.setOrderBy("sd.releaseTime desc");

        model.addAttribute("pageInfo",strategyDetailService.query(qo));

        model.addAttribute("catalogs", strategyCatalogService.list(null));

        model.addAttribute("strategies",strategyService.list());
        return "/strategyDetail/list";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(StrategyDetail entity, MultipartFile img){

        // 先处理上传图片问题
        if(img != null && img.getContentType().startsWith("image") && img.getSize() > 0){
            String url = UploadUtil.upload(img);
            entity.setCoverUrl(url);

        }
        strategyDetailService.saveOrUpdate(entity);
        return "redirect:/strategyDetail/list.do";
    }


    // 根据攻略的id查询攻略内容
    @RequestMapping(value = "/getContentById", produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String getContentById(Long id){
        return strategyDetailService.getContentById(id);
    }

    @RequestMapping("/getStrategyByCatalogId")
    @ResponseBody
    public Object getStrategy(Long CatalogId){
        return strategyDetailService.selectStragyByCatalogId(CatalogId);
    }
}
