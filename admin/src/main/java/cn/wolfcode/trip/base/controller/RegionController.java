package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/region")
public class RegionController {

    @Autowired
    private IRegionService regionService;

    @RequestMapping("/list")
    public String list(QueryObject q0, Model model){
        return "region/list";
    }

    @RequestMapping("/listMenus")
    @ResponseBody
    public Object listMenus(Long parentId, String type) {
        List<Region> list = regionService.listMenus(parentId);
//        List<Map> views = new ArrayList<>();
//        for (Region region : list){
//            views.add(region.toTreeView());
//        }
        // 根据参数判断是表单还是treeView
        if("treeView".equals(type)){
            return list.stream().map(Region::toTreeView).collect(Collectors.toList());
        }
        return list;
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Region entity){
        regionService.saveOrUpdate(entity);
        return "redirect:/region/list.do";
    }
}
