package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.query.StrategyQueryObject;
import cn.wolfcode.trip.base.service.IRegionService;
import cn.wolfcode.trip.base.service.IStrategyService;
import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/strategy")
public class StrategyController {


    @Autowired
    private IStrategyService strategyService;

    @Autowired
    private IRegionService regionService;

    @RequestMapping("/list")
    public String list(@ModelAttribute("qo") StrategyQueryObject qo, Model model){
        // 高级查询所有的大攻略
        model.addAttribute("pageInfo",strategyService.query(qo));
        // 查询所有的区域，模态框回显区域用的
        model.addAttribute("regions", regionService.list(null));
        return "/strategy/list";
    }

    // 因为涉及到文件上传，所以要多传一个参数 MultipartFile
    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Strategy entity, MultipartFile img){

        // 先处理上传图片问题
        if(img != null && img.getContentType().startsWith("image") && img.getSize() > 0){
            String url = UploadUtil.upload(img);
            entity.setCoverUrl(url);
        }
        strategyService.saveOrUpdate(entity);
        return "redirect:/strategy/list.do";
    }
}
