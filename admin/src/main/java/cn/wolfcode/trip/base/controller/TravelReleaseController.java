package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.service.ITravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/travelRelease")
public class TravelReleaseController {

    @Autowired
    private ITravelService travelService;


    // 获得所有游记
    @RequestMapping("/list")
    public Object list(Model model, @ModelAttribute("qo")TravelQueryObject qo){
        // 只查询已发布状态
        qo.setState(Travel.STATE_RELEASE);
        // 按照最近更新时间升序排列
        qo.setOrderBy("t.releaseTime desc");
        // 必须是可公开的状态
        qo.setIsPublic(true);
        model.addAttribute("pageInfo",travelService.queryTravel(qo));
        return "/travelRelease/list";
    }


}
