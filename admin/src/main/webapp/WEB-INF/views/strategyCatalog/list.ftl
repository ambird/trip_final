<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="catalog"/>
    <#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }
        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            $(".inputBtn").click(function () {
               // 先清空数据
                $("#editForm input").val("");

                // 修改的话，需要具体渲染数据来回显
                var json = $(this).data("json");
                if (json) {
                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='name']").val(json.name);
                    $("#editForm input[name='sequence']").val(json.sequence);
                    $("#editForm select[name='strategyId']").val(json.strategyId);
                    // 因为不是字符串，所以拼接一个空字符串
                    $("#editForm select[name='state']").val(json.state+"");
                }


                // 显示模态框
                $("#inputModal").modal("show");
            });

            // 模态框提交
            $(".submitBtn").click(function () {
                $("#editForm").submit();
            });



        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">攻略分类管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategyCatalog/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>攻略搜索:</label>
                    <select class="form-control" name="strategyId">
                        <option value="-1">全部攻略</option>
                        <#list strategies as s>
                            <option value="${s.id}">${s.title}</option>
                        </#list>
                    </select>
                    <script>
                        $("#searchForm select[name='strategyId']").val(${qo.strategyId});
                    </script>
                </div>
                <button class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a role="button" class="btn btn-success inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 新增
                </a>
            </form>


            <table class="table table-striped table-hover">
                <tr>
                    <th>ID</th>
                    <th>分类名称</th>
                    <th>所属攻略</th>
                    <th>序号</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td>${entity.name}</td>
                        <td>${entity.strategyTitle}</td>
                        <td>${entity.sequence}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a role="button" class="btn btn-primary btn-sm inputBtn" data-json='${entity.json}'>
                                <span class="glyphicon glyphicon-pencil"></span> 修改
                            </a>
                        </td>
                    </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>



<#-- 攻略分类管理 -->
<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/strategyCatalog/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id"/>
                    <div class="form-group" >
                        <label class="col-lg-4 control-label">分类名称：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">所属攻略：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="strategyId">
                                <#list strategies as s>
                                    <option value="${s.id}">${s.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">序号：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="sequence"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">状态：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="state">
                                <option value="true">启用</option>
                                <option value="false">禁用</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>