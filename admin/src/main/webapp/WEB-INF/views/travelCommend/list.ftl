<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="commend"/>
    <#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }
        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            // // 查看内容，往模态框中添加游记正文内容
            // $(".inputBtn").click(function () {
            //     // alert(1);
            //     // 找到查看内容按钮，绑定事件
            //     var id = $(this).data("tid");
            //     // ajax请求，根据id获得正文数据
            //     $.get("/travel/getContents.do",{id:id},function (data) {
            //         // 回显到模态框正文部分
            //         $("#contentModal .modal-body").html(data);
            //         // 显示模态框
            //         $("#contentModal").modal("show");
            //     });
            // });

            // 点击设为推荐按钮弹出模态框
            $(".inputBtn").click(function () {
                // 获得json数据
                var json = $(this).data("json");
                console.log(json);
                // 回显数据 id，title,coverUrl
                $("#editForm input[name='id']").val(json.id);
                $("#editForm input[name='travelId']").val(json.id);
                $("#editForm input[name='title']").val(json.title);
                $("#editForm input[name='subTitle']").val(json.subTitle);
                $("#editForm input[name='coverUrl']").val(json.coverUrl);
                $("#editForm input[name='schedule']").val(json.schedule);
                $("#editForm select[name='type']").val(json.type);
                $("#coverUrl").attr("src",json.coverUrl);

                // 动态渲染a标签
                $("#skip").attr("href","/travelRelease/list.do?travelId="+json.travelId);
                // 显示出模态框
                $("#inputModal").modal("show");
            });

            // 模态框中表单提交事件
            $(".submitBtn").click(function () {
                // 选择攻略推荐的特殊提示
                if($("#editForm select[name='type']").val()==3){
                    $.messager.confirm("温馨提示","攻略推荐只能设置一篇，确定要覆盖吗？",function () {
                        $("#editForm").submit();
                    });
                    return;
                }
                $("#editForm").submit();
            });
        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">游记推荐管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/travelCommend/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/副标题">
                </div>
                <div class="form-group">
                    <label>推荐类型:</label>
                    <select class="form-control" name="type">
                        <option value="-1">全部</option>
                        <option value="1">每周推荐</option>
                        <option value="2">每月推荐</option>
                        <option value="3">攻略推荐</option>
                    </select>
                </div>
                <script type="text/javascript">
                    $("#searchForm select[name='type']").val(${qo.type});
                </script>
                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>

            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>封面</th>
                    <th>标题</th>
                    <th>副标题</th>
                    <th>推荐时间</th>
                    <th>推荐类型</th>
                    <th>操作</th>
                </tr>
                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td><img src="${entity.coverUrl}" width="100"/></td>
                        <td>${entity.title}</td>
                        <td>${entity.subTitle}</td>
                        <#--freemarker不能显示时间，所以要格式化-->
                        <td>${entity.schedule?string("yyyy-MM-dd")}</td>
                        <td>${entity.typeName}</td>
                        <td>

                            <a role="button" class="btn btn-primary btn-sm inputBtn" data-json='${entity.json}'>
                                <span class="glyphicon glyphicon-ok"></span> 修改
                            </a>



                        </td>
                    </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">游记内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>


<#--修改模态框-->
<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">游记内容</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/travelCommend/saveOrUpdate.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="travelId"/>
                    <input type="hidden" name="id"/>
                    <input type="hidden" name="coverUrl"/>
                    <div class="form-group" >
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">副标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="subTitle"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">封面：</label>
                        <div class="col-lg-6">
                            <img id="coverUrl" style="width: 100%"/>
                            <input type="file" class="form-control" name="img"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">推荐时间：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="schedule"
                                   onclick="WdatePicker({readOnly:true});"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">推荐类型：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="type">
                                <option value="1">每周推荐</option>
                                <option value="2">每月推荐</option>
                                <option value="3">攻略推荐</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <a id="skip" target="_blank" class="btn btn-success">
                            <span class="glyphicon glyphicon-sunglasses"></span>
                            查看游记详细信息
                        </a>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
</body>
</html>