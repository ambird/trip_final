<ul id="menu" class="list-group">
	<li class="list-group-item">
		<a href="javascript:" data-toggle="collapse" data-target="#user_detail">
			<span>平台管理</span>
		</a>
		<ul class="in" id="user_detail">
			<li class="user"><a href="/user/list.do">注册用户管理</a></li>
		</ul>
		<ul class="in" id="user_detail">
			<li class="systemMsg"><a href="/systemMsg/list.do">系统通知管理</a></li>
		</ul>
		<ul class="in" id="region_detail">
			<li class="region"><a href="/region/list.do">旅游区域管理</a></li>
		</ul>
	</li>
    <li class="list-group-item">
        <a href="javascript:" data-toggle="collapse" data-target="#travel_detail">
            <span>游记管理</span>
        </a>
        <ul class="in" id="travel_detail">
            <li class="travel"><a href="/travel/list.do">待发布游记管理</a></li>
        </ul>
        <ul class="in" id="travel_detail">
            <li class="release"><a href="/travelRelease/list.do">已发布游记管理</a></li>
        </ul>
        <ul class="in" id="travel_detail">
            <li class="commend"><a href="/travelCommend/list.do">游记推荐管理</a></li>
        </ul>
    </li>
    <li class="list-group-item">
        <a href="javascript:" data-toggle="collapse" data-target="#strategy_detail">
            <span>攻略管理</span>
        </a>
        <ul class="in" id="strategy_detail">
            <li class="strategy"><a href="/strategy/list.do">大攻略管理</a></li>
        </ul>
        <ul class="in" id="strategy_detail">
            <li class="catalog"><a href="/strategyCatalog/list.do">攻略分类管理</a></li>
        </ul>
        <ul class="in" id="strategy_detail">
            <li class="strategyDetail"><a href="/strategyDetail/list.do">攻略文章管理</a></li>
        </ul>
    </li>
    <li class="list-group-item">
        <a href="javascript:" data-toggle="collapse" data-target="#hotel_detail">
            <span>酒店管理</span>
        </a>
        <ul class="in" id="hotel_detail">
            <li class="hotel"><a href="/hotel/list.do">酒店明细管理</a></li>
        </ul>
        <ul class="in" id="hotel_detail">
            <li class="hotelOrder"><a href="/hotelOrder/list.do">酒店订单管理</a></li>
        </ul>
    </li>
    <li class="list-group-item">
        <a href="javascript:" data-toggle="collapse" data-target="#news_detail">
            <span>日报管理</span>
        </a>
        <ul class="in" id="news_detail">
            <li class="news"><a href="/news/list.do">待发布日报管理</a></li>
        </ul>
        <ul class="in" id="news_detail">
            <li class="newsRelease"><a href="/newsRelease/list.do">已发布日报管理</a></li>
        </ul>
        <ul class="in" id="news_detail">
            <li class="newsHead"><a href="/newsHead/list.do">头条日报管理</a></li>
        </ul>
    </li>
    <li class="list-group-item">
        <a href="javascript:" data-toggle="collapse" data-target="#news_detail">
            <span>商品管理</span>
        </a>
        <ul class="in" id="news_detail">
            <li class="product"><a href="/product/list.do">商品管理</a></li>
        </ul>
    </li>

</ul>

<script type="text/javascript">
    $(".in li.${currentMenu}").addClass("active");
</script>
