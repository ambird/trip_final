<div style="text-align: center;">
    <ul id="pagination" class="pagination"></ul>
</div>
<script type="text/javascript">
    $(function(){
        $("#pagination").twbsPagination({
            //这里填充数据使用的是PageInfo
            totalPages: ${pageInfo.pages} || 1,
            startPage: ${pageInfo.pageNum}||1,
            visiblePages: 5,
            first:"首页",
            prev:"上页",
            next:"下页",
            last:"尾页",
            onPageClick:function(event, page){
                //讲当前页设置page值
                $("#currentPage").val(page);
                //表单提交
                $("#searchForm").submit();
            }
        });

        $(".deleteBtn").click(function () {
            var url = $(this).data("url");
            $.messager.confirm("温馨提示", "亲,您确定要删除吗?", function () {
                $.get(url, function (data) {
                    location.reload();
                });
            });
        });
    });
</script>