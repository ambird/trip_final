<!DOCTYPE html>
<html lang="zh_CN">
<head>
<#assign currentMenu="systemMsg"/>
<#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <script src="/js/plugins/My97DatePicker/WdatePicker.js"></script>
    <script src="/js/plugins/ckeditor/ckeditor.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }

        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            //初始化一个ckeditor
            var edt = CKEDITOR.replace("ckeditor");
            var edt = CKEDITOR.replace("ckeditor1");

            //给查看内容按钮添加一个点击事件
            $(".contentBtn").click(function () {
                //获取当前的点击按钮
                var id = $(this).data("tid");
                //发送ajax请求,根据获取到的id查询日报内容
                $.get("/systemMsg/getContent.do", {id: id}, function (data) {
                    $("#contentModal .modal-body").html(data);
                    //弹出内容的模态框
                    $("#contentModal").modal('show');
                })
            })

            //给发送全部按钮添加一个点击事件
            $(".allBtn").click(function () {
                //弹出模态框
                $("#allModal").modal('show');
            })

            //提交全部人表单
            $(".submitBtn").click(function () {
                $("#editsForm").submit();
            })

            //给发送个人的按钮添加一个点击事件
            $(".personBtn").click(function () {
                //弹出提示信息
                $.messager.confirm("温馨提示", "您是否记住了要发送给谁的邮箱,如果记住,请点击OK,否则点击Cancel," +
                        "再去注册用户管理查询所要发送的邮箱", function () {
                    //弹出模态框
                    $("#personModal").modal('show');
                })
            })

            //提交个人的表单
            $(".submitBtn1").click(function () {
                $("#editForm").submit();
            })
        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
<#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
        <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">日报发布管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/systemMsg/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题">
                </div>
                <div class="form-group">
                    <label>通知类型:</label>
                    <select class="form-control" name="state">
                        <option value="-1">全部</option>
                        <option value="0">全部人</option>
                        <option value="1">个人</option>
                    </select>
                </div>
                <script type="text/javascript">
                    $("#searchForm select[name='state']").val(${(qo.state)!});
                </script>
                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a role="button" class="btn btn-default btn-info allBtn">
                    <span class="glyphicon glyphicon-plus"></span> 发送给全部人
                </a>
                <a role="button" class="btn btn-default btn-info personBtn">
                    <span class="glyphicon glyphicon-plus"></span> 发送给个人
                </a>
            </form>

            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>标题</th>
                    <th>通知时间</th>
                    <th>状态</th>
                    <th>邮箱</th>
                    <th>昵称</th>
                    <th>操作</th>
                </tr>
            <#list pageInfo.list as entity>
                <tr>
                    <td>${entity_index + 1}</td>
                    <td>${entity.title}</td>
                    <td>${entity.time?string("yyyy-MM-dd HH-mm-ss")}</td>
                    <td>${entity.stateName}</td>
                    <td>${(entity.user.email)!}</td>
                    <td>${(entity.user.nickName)!}</td>
                    <td>
                        <a role="button" class="btn btn-primary btn-sm contentBtn" data-tid="${entity.id}">
                            <span class="glyphicon glyphicon-sunglasses"></span> 查看发送的内容
                        </a>
                    </td>
                </tr>
            </#list>
            </table>
        <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">系统消息内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>


<#--发送给全部人模态框-->
<div id="allModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">发送内容</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/systemMsg/save.do" method="post"
                      enctype="multipart/form-data" id="editsForm">
                    <input type="hidden" name="state" value="0">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="content" id="ckeditor"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<#--发送给个人模态框-->
<div id="personModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">发送内容</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/systemMsg/save.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="state" value="1">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">发送给：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="user.email" placeholder="请输入想要发送给谁的邮箱"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="content" id="ckeditor1"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn1">保存</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
</body>
</html>