<!DOCTYPE html>
<html lang="zh_CN">
<head>
<#assign currentMenu="news"/>
<#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <script src="/js/plugins/My97DatePicker/WdatePicker.js"></script>
    <script src="/js/plugins/ckeditor/ckeditor.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }

        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            // 初始化一个ckeditor
            var edt = CKEDITOR.replace("ckeditor");

            //给查看内容按钮添加一个点击事件
            $(".contentBtn").click(function () {
                //获取当前的点击按钮
                var id = $(this).data("tid");
                //发送ajax请求,根据获取到的id查询日报内容
                $.get("/news/getContent.do", {id: id}, function (data) {
                    $("#contentModal .modal-body").html(data);
                    //弹出内容的模态框
                    $("#contentModal").modal('show');
                })

            })

            //给添加增加一个点击事件
            $(".inputBtn").click(function () {
                //把模态框清除
                $("#editForm input").val("");
                $("#coverUrl").attr("src", "");
                //获取当前的点击事件
                var json = $(this).data("json");
                if (json) {
                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='title']").val(json.title);
                    $("#editForm input[name='subTitle']").val(json.subTitle);
                    $("#coverUrl").attr("src", json.coverUrl);
                    //发送ajax请求,根据获取到的id查询日报内容
                    $.get("/news/getContent.do", {id: json.id}, function (data) {
                        edt.setData(data);
                    }, "html")

                }

                //弹出模态框
                $("#inputModal").modal('show');
            })

            //给发布按钮添加一个点击事件
            $(".changeStateBtn").click(function () {
                //获取当前的点击对象
                var json = $(this).data("json");
                $.messager.confirm("温馨提示", "你确定要发布这篇日报吗?", function () {
                    //发送ajax请求,请求更新状态
                    $.post("/news/changeState.do", json, function (data) {
                        location.reload();
                    })
                })
            })


            //提交表单
            $(".submitBtn").click(function () {
                $("#ckeditor").val(edt.getData());
                $("#editForm").submit();
            })
        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
<#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
        <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">日报发布管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/news/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题">
                </div>
                <div class="form-group">
                    <label>发布类型:</label>
                    <select class="form-control" name="state">
                        <option value="-1">全部</option>
                        <option value="1">未发布</option>
                        <option value="0">下架</option>
                    </select>
                </div>
                <script type="text/javascript">
                    $("#searchForm select[name='state']").val(${(qo.state)!});
                </script>
                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a role="button" class="btn btn-default btn-info inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 新增
                </a>
            </form>

            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>封面</th>
                    <th>标题</th>
                    <th>副标题</th>
                    <th>创建时间</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
            <#list pageInfo.list as entity>
                <tr>
                    <td>${entity_index + 1}</td>
                    <td><img src="${entity.coverUrl}" width="100"/></td>
                    <td>${entity.title}</td>
                    <td>${entity.subTitle}</td>
                <#--freemarker不能显示时间，所以要格式化-->
                    <td>${entity.createTime?string("yyyy-MM-dd")}</td>
                    <td>${entity.stateName}</td>
                    <td>
                        <a role="button" class="btn btn-success btn-sm changeStateBtn"
                           data-json='{"id":${entity.id}, "state": 2}'>
                            <span class="glyphicon glyphicon-ok"></span> 发布
                        </a>
                        <a role="button" class="btn btn-primary btn-sm inputBtn" data-json='${entity.json}'>
                            <span class="glyphicon glyphicon-pencil"></span> 修改
                        </a>
                        <a role="button" class="btn btn-primary btn-sm contentBtn" data-tid="${entity.id}">
                            <span class="glyphicon glyphicon-sunglasses"></span> 查看内容
                        </a>
                    </td>
                </tr>
            </#list>
            </table>
        <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">日报内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>


<#--新增或编辑模态框-->
<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">日报添加或编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/news/saveOrUpdate.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="travelId"/>
                    <input type="hidden" name="id"/>
                    <input type="hidden" name="coverUrl"/>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">副标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="subTitle"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">封面：</label>
                        <div class="col-lg-6">
                            <img id="coverUrl" style="width: 100%"/>
                            <input type="file" class="form-control" name="img"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="newsContent.content" id="ckeditor"></textarea>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
</body>
</html>