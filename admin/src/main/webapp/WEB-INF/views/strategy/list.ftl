<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="strategy"/>
    <#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }
        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            $(".inputBtn").click(function () {
               // 先清空数据
                $("#editForm input").val("");
                $("#coverUrl").attr("src","");

                // 修改的话，需要具体渲染数据来回显
                var json = $(this).data("json");
                if (json) {
                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='title']").val(json.title);
                    $("#editForm input[name='subTitle']").val(json.subTitle);
                    $("#coverUrl").attr("src",json.coverUrl);
                    $("#editForm select[name='place.id']").val(json.placeId);
                    $("#editForm select[name='state']").val(json.state);
                }

                // 显示模态框
                $("#inputModal").modal("show");
            });

            // 模态框提交
            $(".submitBtn").click(function () {
                $("#editForm").submit();
            });



        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">大攻略管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategy/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/副标题">
                </div>
                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a role="button" class="btn btn-default btn-info inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 新增
                </a>
            </form>


            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>封面</th>
                    <th>标题</th>
                    <th>副标题</th>
                    <th>区域</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td><img src="${entity.coverUrl}" width="100"/></td>
                        <td>${entity.title}</td>
                        <td>${entity.subTitle}</td>
                        <td>${entity.place.name}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a role="button" class="btn btn-primary btn-sm inputBtn" data-json='${entity.json}'>
                                <span class="glyphicon glyphicon-pencil"></span> 修改
                            </a>
                        </td>
                    </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>



<#--添加/修改模态框-->
<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/strategy/saveOrUpdate.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="id"/>
                    <div class="form-group" >
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">副标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="subTitle"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">封面：</label>
                        <div class="col-lg-6">
                            <img id="coverUrl" style="width: 100%"/>
                            <input type="file" class="form-control" name="img"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">区域：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="place.id">
                                <#list regions as r>
                                    <option value="${r.id}">${r.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">状态：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="state">
                                <option value="0">正常</option>
                                <option value="1">禁用</option>
                                <option value="2">推荐</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>