<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="hotel"/>
    <#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }

        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            //当点击添加的时候,需要弹出一个模态框
            $("#btn_add").click(function () {


                $("#inputModal").modal("show");
            })

            $(".submitBtn").click(function () {
                //直接提交表单
                $("#editForm").submit();
            })

            //当点击修改的时候,会提交表单
            $(".inputBtn").click(function () {
                var json = $(this).data("json");
                //接下来一个一个回显数据
                console.log(json);
                if (json) {
                    $("#editForm input[name='name']").val(json.name);
                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='title']").val(json.title);
                    $("#editForm select[name='place.id']").val(json.placeId);
                    //回显封面,需要特殊处理
                    $("#coverUrl").prop("src", json.coverUrl);
                    $("#editForm input[name='pubPrice']").val(json.pubPrice);
                    $("#editForm input[name='vipPrice']").val(json.vipPrice);
                    $("#editForm input[name='perExpends']").val(json.perExpends);
                    $("#editForm input[name='state']").val(json.state);
                    $("#inputModal").modal("show");
                }
            })

            //提交表单
            $(".submitBtn").click(function () {
                //直接提交表单
                $("#editForm").submit();
            })
        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-3">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">酒店管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/hotel/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/作者">
                </div>

                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>
            <div class="form-group">
                <button id="btn_add" class="btn btn-default btn-info">
                    <span class="glyphicon glyphicon-adjust"></span> 添加
                </button>
            </div>
            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>酒店名称</th>
                    <th>酒店标题</th>
                    <th>所在区域</th>
                    <th>封面</th>
                    <th>普通价格</th>
                    <th>会员价格</th>
                    <th>推荐等级</th>
                    <th>平均价格</th>
                    <th>操作</th>
                </tr>
                <#list pageInfo.list as entity>
                        <td>${entity_index + 1}</td>
                        <td>${entity.name}</td>
                        <td>${entity.title}</td>
                        <td>${(entity.place.name)!}</td>
                        <td><img src="${entity.coverUrl}" width="100"/></td>
                        <td>${entity.pubPrice}</td>
                        <td>${entity.vipPrice}</td>
                        <td>${entity.getStateName()}</td>
                        <td>${entity.perExpends}</td>
                        <td>

                            <a id="btn_add" role="button" class="btn btn-success btn-sm inputBtn"
                               data-json='${(entity.json)!}'>
                                <span class="glyphicon glyphicon-camera"></span> 修改
                            </a>

                        </td>
                    </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">酒店内容</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/hotel/saveOrUpdate.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="id"/>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">酒店名称：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">区域：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="place.id">
                                <#list regions as r>
                                    <option value="${r.id}">${r.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">封面：</label>
                        <div class="col-lg-6">
                            <img id="coverUrl" style="width: 100%"/>
                            <input type="file" class="form-control" name="img"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">普通价格：</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="pubPrice"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">会员价格：</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="vipPrice"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">平均价格：</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="perExpends"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">推荐类型：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="state">
                                <option value="1">普通</option>
                                <option value="2">推荐</option>
                                <option value="3">强烈推荐</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>
</body>
</html>