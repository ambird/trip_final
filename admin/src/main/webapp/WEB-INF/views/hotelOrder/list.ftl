<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="hotelOrder"/>
    <#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }

        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            //当点击添加的时候,需要弹出一个模态框
      $("#btn_add").click(function () {


                $("#inputModal").modal("show");
            })

            $(".submitBtn").click(function () {
                //直接提交表单
                $("#editForm").submit();
            })

            //当点击修改的时候,会提交表单
         $(".inputBtn").click(function () {
                var json = $(this).data("json");
             console.log(json);
             //接下来一个一个回显数据
                console.log(json);
                if (json) {
                    $("#editForm input[name='customerName']").val(json.customerName);
                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='tel']").val(json.tel);
                   /* $("#editForm select[name='region.id']").val(json.regionId);*/
                    $("#editForm select[name='hotel.id']").val(json.hotelId);
                    //回显封面,需要特殊处理
                    $("#editForm select[name='gender']").val(json.gender);
                    $("#editForm input[name='pubPrice']").val(json.pubPrice);
                    $("#editForm input[name='liveTime']").val(json.liveTime);
                    $("#editForm input[name='outTime']").val(json.outTime);

                    //弹出模态框
                    $("#inputModal").modal("show");
                }
            });

            //提交表单
            $(".submitBtn").click(function () {
                //直接提交表单
                $("#editForm").submit();
            })
        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-3">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">酒店订单管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/hotelOrder/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/作者">
                </div>

                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>
            <div class="form-group">
                <button id="btn_add" class="btn btn-default btn-info">
                    <span class="glyphicon glyphicon-adjust"></span> 添加
                </button>
            </div>
            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>酒店名称</th>
                    <th>客户姓名</th>
                    <th>客户电话</th>
                    <th>房间价格</th>
                    <th>性别</th>
                    <th>进店时间</th>
                    <th>离店时间</th>
                    <th>酒店位置</th>
                </tr>
                <#list pageInfo.list as entity>
                        <td>${entity_index + 1}</td>
                        <td>${entity.hotel.name}</td>
                        <td>${entity.customerName}</td>
                        <td>${(entity.tel)!}</td>
                        <td>${entity.hotel.pubPrice}</td>
                        <td>${entity.getGenderName()}</td>
                        <td>${entity.liveTime?string("yyyy-MM-dd")}</td>
                        <td>${entity.outTime?string("yyyy-MM-dd")}</td>
                        <td>

                            <a id="btn_add" role="button" class="btn btn-success btn-sm inputBtn"
                               data-json='${(entity.json)!}'>
                                <span class="glyphicon glyphicon-camera"></span> 修改
                            </a>

                        </td>
                    </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">酒店订单</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/hotelOrder/saveOrUpdate.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="id"/>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">顾客姓名：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control " name="customerName" readonly="true" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">联系电话：</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="tel"/>
                        </div>
                    </div>

                    <#--<div class="form-group">
                        <label class="col-lg-4 control-label">区域：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="region.id">
                                <#list regions as r>
                                    <option value="${r.id}">${r.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label class="col-lg-4 control-label">酒店名称：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="hotel.id">
                                <#list hotels as h>
                                    <option value="${h.id}">${h.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">性别：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="gender" >
                                <option value="-1">保密</option>
                                <option value="1">男</option>
                                <option value="0">女</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">房间价格：</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="pubPrice"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">进店时间：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="liveTime"
                                   onclick="WdatePicker({readOnly:true});"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">离店时间：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="outTime"
                                   onclick="WdatePicker({readOnly:true});"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>
</body>
</html>