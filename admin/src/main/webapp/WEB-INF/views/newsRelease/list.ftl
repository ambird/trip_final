<!DOCTYPE html>
<html lang="zh_CN">
<head>
<#assign currentMenu="newsRelease"/>
<#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }

        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {

        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
<#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
        <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">已发布游记管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/newsRelease/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/副标题">
                </div>
                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>

            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>封面</th>
                    <th>标题</th>
                    <th>副标题</th>
                    <th>点赞数</th>
                    <th>状态</th>
                    <th>发布时间</th>
                    <th>操作</th>
                </tr>
            <#list pageInfo.list as entity>
                <td>${entity_index + 1}</td>
                <td><img src="${entity.coverUrl}" width="100"/></td>
                <td>${entity.title}</td>
                <td>${entity.subTitle}</td>
                <td>${entity.good}</td>
                <td>${entity.stateName}</td>
                <td>${(entity.releaseTime?string("yyyy-MM-dd"))!}</td>
                <td>
                    <a role="button" class="btn btn-primary btn-sm contentBtn" data-tid="${entity.id}">
                        <span class="glyphicon glyphicon-sunglasses"></span> 查看内容
                    </a>

                    <a role="button" class="btn btn-danger btn-sm changeStateBtn"
                       data-json='{"id":${entity.id}, "state": 0}'>
                        <span class="glyphicon glyphicon-ok"></span> 下架
                    </a>
                    <a role="button" class="btn btn-success btn-sm changeStateBtn"
                       data-json='{"id":${entity.id}, "state": 3}'>
                        <span class="glyphicon glyphicon-sm"></span> 设为头条
                    </a>
                </td>
                </tr>
            </#list>
            </table>
        <#include "../common/page.ftl">
        </div>
    </div>
</div>
<#--查看内容模态框-->
<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">日报内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        //给查看内容按钮添加一个点击事件
        $(".contentBtn").click(function () {
            //获取当前的点击按钮
            var id = $(this).data("tid");
            //发送ajax请求,根据获取到的id查询日报内容
            $.get("/news/getContent.do", {id: id}, function (data) {
                $("#contentModal .modal-body").html(data);
                //弹出内容的模态框
                $("#contentModal").modal('show');
            })
        })

        //给下架和设为头条按钮添加一个点击事件
        $(".changeStateBtn").click(function () {
            //获取当前点击的对象
            var json = $(this).data("json");
            //温馨提示
            $.messager.confirm("温馨提示","头条只能设置一条,你确定要执行此操作吗?",function () {
                //发送ajax请求,请求更新状态
                $.post("/news/changeState.do",json,function (data) {
                    location.reload();
                })
            })
        })


    })
</script>
</body>
</html>