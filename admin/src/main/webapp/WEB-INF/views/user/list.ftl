<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl">
    <script type="text/javascript">
        $(function () {

        });
    </script>
</head>
<body>
<#assign currentMenu="user">
<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">用户管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/user/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label for="keyword">关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入昵称/邮箱">
                </div>
                <button id="btn_query" class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>

            <table class="table table-striped table-hover">
                <tr>
                    <th>邮箱</th>
                    <th>昵称</th>
                    <th>地区</th>
                    <th>头像</th>
                    <th>性别</th>
                    <th>签名</th>
                </tr>
                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity.email}</td>
                        <td>${entity.nickName}</td>
                        <td>${entity.place}</td>
                        <td><img src="${entity.headImgUrl}" width="20"</td>
                        <td>${entity.genderName}</td>
                        <td>${entity.sign}</td>
                    </tr>

                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>
</body>
</html>