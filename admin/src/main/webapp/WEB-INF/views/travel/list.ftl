<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="travel"/>
    <#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }
        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            // 查看内容，往模态框中添加游记正文内容
            $(".contentBtn").click(function () {
                // alert(1);
                // 找到查看内容按钮，绑定事件
                var id = $(this).data("tid");
                // ajax请求，根据id获得正文数据
                $.get("/travel/getContents.do",{id:id},function (data) {
                    // 回显到模态框正文部分
                    $("#contentModal .modal-body").html(data);
                    // 显示模态框
                    $("#contentModal").modal("show");
                });
            });

            // 发布和拒绝
            $(".changeStateBtn").click(function () {
                var json = $(this).data("json");

                $.messager.confirm("温馨提示","确定当前操作吗?",function () {
                    // 发送ajax请求,执行更新状态
                    $.post("/travel/changeState.do",json,function (data) {
                        location.reload();
                    });
                });
            });

        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">待发布游记管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/travel/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/作者">
                </div>
                <div class="form-group">
                    <label>状态:</label>
                    <select class="form-control" name="state">
                        <option value="-2">全部</option>
                        <option value="-1">已拒绝</option>
                        <option value="1">待审核</option>
                    </select>
                </div>

                <script type="text/javascript">
                    $("#searchForm select[name='state']").val(${qo.state});
                </script>

                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>

            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>封面</th>
                    <th>标题</th>
                    <th>区域</th>
                    <th>作者</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td><img src="${entity.coverUrl}" width="100"/></td>
                        <td>${entity.title}</td>
                        <td>${entity.place.name}</td>
                        <td>${entity.author.nickName}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <#if entity.state == 1>
                                <a role="button" class="btn btn-success btn-sm changeStateBtn" data-json='{"id":${entity.id}, "state": 2}'>
                                    <span class="glyphicon glyphicon-ok"></span> 发布
                                </a>
                                <a role="button" class="btn btn-danger btn-sm changeStateBtn" data-json='{"id":${entity.id}, "state": -1}'>
                                    <span class="glyphicon glyphicon-remove"></span> 拒绝
                                </a>
                            </#if>
                            <a role="button" class="btn btn-primary btn-sm contentBtn" data-tid="${entity.id}">
                                <span class="glyphicon glyphicon-sunglasses"></span> 查看内容
                            </a>
                        </td>
                    </tr>
                </#list>
            </table>
            <#include "../common/page.ftl">
        </div>
    </div>
</div>

<div id="contentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">游记内容</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-info" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
</script>
</body>
</html>