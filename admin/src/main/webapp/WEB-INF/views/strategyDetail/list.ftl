<!DOCTYPE html>
<html lang="zh_CN">
<head>
<#assign currentMenu="strategyDetail"/>
<#include "../common/header.ftl">
    <script src="/js/plugins/jquery-bootstrap/jquery.bootstrap.min.js"></script>
    <script src="/js/plugins/ckeditor/ckeditor.js"></script>
    <style type="text/css">
        .modal-body {
            max-height: 750px;
            overflow-y: auto;
        }

        .modal-body img {
            width: 100%;
        }
    </style>
    <script>
        $(function () {
            // 初始化一个ckeditor
            var edt = CKEDITOR.replace("ckeditor");

            // 发送ajax请求，查询默认第一位的攻略对应的分类
            linkage($("#strategy option:first").val());

            // 当大攻略发生改变，重新发送ajax请求，查询分类
            $("#strategy").change(function () {
                linkage(this.value);
            });

            $(".inputBtn").click(function () {
                // 先清空数据
                $("#editForm input").val("");
                $("#coverUrl").attr("src", "");


                // 修改的话，需要具体渲染数据来回显
                var json = $(this).data("json");
                if (json) {
                    $.get("/strategyDetail/getStrategyByCatalogId.do",{CatalogId: json.catalogId},function (data) {
                        console.log(data);
                        linkage(data);
                        $("#strategy").val(data);
                    })

                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='title']").val(json.title);
                    $("#editForm input[name='sequence']").val(json.sequence);
                    $("#coverUrl").attr("src", json.coverUrl);
                    $("#editForm select[name='state']").val(json.state);
                    setTimeout(function () {
                        $("#catalog").val(json.catalogId);
                    }, 50);

                    $.get("/strategyDetail/getContentById.do", {id: json.id}, function (data) {
                        // 把文章的内容回显到editor
                        edt.setData(data);
                    }, "html");
                }
                // 显示模态框
                $("#inputModal").modal("show");
            });


            // 模态框提交
            $(".submitBtn").click(function () {
                // 把ckeditor的数据填充到文本中，再提交表单
                $("#ckeditor").html(edt.getData());
                $("#editForm").submit();
            });

        });

        // 二级联动
        function linkage(strategyId) {
            $.get("/strategyCatalog/listByStrategyId.do", {strategyId: strategyId}, function (data) {
                $("#catalog").html("");
                $.each(data, function (index, ele) {
                    $("<option></option>").val(ele.id).html(ele.name).appendTo("#catalog");
                });
            });
        }
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
<#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
        <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">攻略文章管理</h1>
                </div>
            </div>
            <!--高级查询--->
            <form class="form-inline" id="searchForm" action="/strategyDetail/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword" value="${(qo.keyword)!}"
                           placeholder="请输入标题/分类">
                </div>
                <button id="btn_query" class="btn btn-default btn-success">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a role="button" class="btn btn-default btn-info inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 新增
                </a>
            </form>


            <table class="table table-striped table-hover">
                <tr>
                    <th>序号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>发布时间</th>
                    <th>排序</th>
                    <th>攻略分类</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
            <#list pageInfo.list as entity>
                <tr>
                    <td>${entity_index + 1}</td>
                    <td>${entity.title}</td>
                    <td><img src="${entity.coverUrl}" width="100"/></td>
                    <td>${entity.releaseTime?string("yyyy-MM-dd HH:mm")}</td>
                    <td>${entity.sequence}</td>
                    <td>${entity.catalog.name}</td>
                    <td>${entity.stateName}</td>
                    <td>
                        <a role="button" class="btn btn-primary btn-sm inputBtn" data-json='${entity.json}'>
                            <span class="glyphicon glyphicon-pencil"></span> 修改
                        </a>
                    </td>
                </tr>
            </#list>
            </table>
        <#include "../common/page.ftl">
        </div>
    </div>
</div>


<#-- strategyDetail -->
<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/strategyDetail/saveOrUpdate.do" method="post"
                      enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="id"/>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">标题：</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">封面：</label>
                        <div class="col-lg-6">
                            <img id="coverUrl" style="width: 100%"/>
                            <input type="file" class="form-control" name="img"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">所属攻略：</label>
                        <div class="col-lg-6">
                            <select class="form-control" id="strategy">
                            <#list strategies as s>
                                <option value="${s.id}">${s.title}</option>
                            </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">攻略类别：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="catalog.id" id="catalog">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">序列：</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="sequence"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">状态：</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="state">
                                <option value="0">发布</option>
                                <option value="1">草稿</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="strategyContent.content" id="ckeditor"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary submitBtn">保存</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>