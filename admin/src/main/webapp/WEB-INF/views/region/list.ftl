<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#assign currentMenu="region"/>
    <#include "../common/header.ftl">
    <link rel="stylesheet" href="/js/plugins/treeView/bootstrap-treeview.min.css" type="text/css" />
    <script type="text/javascript" src="/js/plugins/treeView/bootstrap-treeview.min.js"></script>
    <script type="text/javascript">
        $(function () {
            // 先克隆表格中的行数据
            var tr = $("tbody tr:first").clone();
            // 页面加载完毕发送ajax请求，拿到所有根区域
            $.get("/region/listMenus.do",{type:"treeView"},function (rootMenus) {
                $("#treeview").treeview({
                    color:"#428bca",
                    showTags:true,
                    data:[{text:"所有区域", nodes:rootMenus}],
                    // 点击+的时候，懒加载
                    lazyLoad : function (node) {
                        // node就是我当前点击的节点
                        // 发送ajax请求查询该区域的子区域
                        $.get("/region/listMenus.do",{parentId:node.id, type:"treeView"},function (childMenus) {
                            // 添加节点
                           $("#treeview").treeview('addNode',[childMenus,node]);
                        });
                    },


                    // 点击节点触发的事件
                    onNodeSelected: function (ev,node) {
                        // 显示表格
                        $("table").css("display","");
                        // 发送ajax请求查询该区域的子区域
                        $.get("/region/listMenus.do",{parentId:node.id},function (childMenus) {
                            // 先清空原来的数据，再渲染
                            $("tbody").html("");
                            $.each(childMenus,function (index,ele) {
                                console.log(ele);
                                var row = tr.clone();
                                row.find("td")[0].innerHTML = index + 1;
                                row.find("td")[1].innerHTML = ele.name;
                                // 自定义属性，用于编辑的时候，数据回显
                                row.find("a").attr("data-json",JSON.stringify(ele));
                                row.appendTo("tbody");
                            });
                        });

                    },

                    // 取消节点选择触发的事件
                    onNodeUnselected:function (ev,node) {
                        $("table").css("display","none");
                    }




                });
            });

            // 添加点击时，弹出模态框
            $("#inputBtn").click(function () {
                showModal();
                // 回显当前选中的区域数据到模态框
                var node = $("#treeview").treeview('getSelected')[0];
                if(node){

                    $("#editForm input[name='parent.name']").val(node.text);
                    $("#editForm input[name='parent.id']").val(node.id);
                } else {
                    $("#editForm input[name='parent.name']").val("所有区域");
                }
            });

            // 由于编辑按钮是动态生成的，常规绑定方式是无效的，必须使用动态事件绑定
            // 参数一：事件 参数二：选择器 参数三：响应函数
            $("tbody").on("click",".inputBtn", function () {
                // 显示模态框，回显数据
                var json = $(this).data("json");
                showModal(json);
            })


            // 显示模态框
            function showModal(json) {
                // 先清空数据
                $("#inputModal input").val("");

                // 如果有json，表示是编辑
                if(json){
                    $("#editForm input[name='id']").val(json.id);
                    $("#editForm input[name='name']").val(json.name);
                    $("#editForm select[name='state']").val(json.state);
                }
                var node = $("#treeview").treeview('getSelected')[0];
                // 回显父级区域
                if(node){
                    $("#editForm input[name='parent.name']").val(node.text);
                    $("#editForm input[name='parent.id']").val(node.id);
                } else {
                    $("#editForm input[name='parent.name']").val("所有区域");
                }
                // show
                $("#inputModal").modal("show");
            };



            // 点击表单提交按钮，提交事件
            $(".submitBtn").click(function () {
                $("#editForm").submit();

            });
        });
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl">
    <div class="row">
        <div class="col-lg-2">
            <#include "../common/menu.ftl">
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-head-line">旅游区域管理</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <a role="button" class="btn btn-success" id="inputBtn">
                        <span class="glyphicon glyphicon-plus"></span> 添加区域
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div id="treeview"></div>
                </div>
                <div class="col-lg-8">
                    <table class="table table-striped table-hover" style="display: none">
                        <thead>
                            <tr>
                                <th>序号</th>
                                <th>区域</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    <a href="javascript:" class="btn btn-info btn-xs inputBtn">
                                        <span class="glyphicon glyphicon-pencil"></span> 修改
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/region/saveOrUpdate.do">
                    <input type="hidden" name="id" value="" />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">区域名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" placeholder="请输入区域名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">上级区域</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="parent.name" readonly/>
                            <input type="hidden" class="form-control" name="parent.id" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">设为推荐</label>
                        <div class="col-sm-6">
                            <select name="state" class="form-control">
                                <option value="0">普通</option>
                                <option value="1">推荐</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:" class="btn btn-success submitBtn" aria-hidden="true">保存</a>
                <a href="javascript:" class="btn btn-info" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>