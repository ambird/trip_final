package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.HotelOrder;
import cn.wolfcode.trip.base.service.IHotelOrderService;
import cn.wolfcode.trip.base.service.IHotelService;
import cn.wolfcode.trip.base.util.HotelQueryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

// 相当于@Controller + @ResponseBody
@RestController
// 提取出来"/users"
@RequestMapping("/hotels")
public class HotelController {
    @Autowired
    private IHotelService hotelService;

    //注入订单对象
    @Autowired
    private IHotelOrderService hotelOrderService;


    /* //查询酒店相应的数据来获取状态为1的酒店
     @GetMapping()
     public Object listHotels(Integer state) {

         return hotelService.listHotel(state);
     }
 */
    //酒店为固定的信息
    @GetMapping("/{id}")
    public Object getHotels(@PathVariable Long id) {

        return hotelService.getHotels(id);
    }

    //酒店为固定的信息
    @GetMapping
    public Object getAllHotels(HotelQueryObject qo) {

        return hotelService.getAllHotels(qo);
    }

    //酒店为固定的信息
    @PostMapping
    public void saveHotel(HotelOrder entity) {

        hotelOrderService.saveOrUpdate(entity);
    }


}
