package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.query.StrategyQueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import cn.wolfcode.trip.base.service.IStrategyDetailService;
import cn.wolfcode.trip.base.service.IStrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 大攻略资源的的控制器
@RestController
@RequestMapping("/strategies")
public class StrategyController {
    @Autowired
    private IStrategyService strategyService;

    @Autowired
    private IStrategyCatalogService strategyCatalogService;

    @Autowired
    private IStrategyDetailService strategyDetailService;

    @GetMapping
    public Object getCommends(StrategyQueryObject qo){
        return strategyService.query(qo);
    }

    // 根据攻略的id查询大攻略
    @GetMapping("/{id}")
    public Object getStrategyById(@PathVariable Long id){
        return strategyService.get(id);
    }

    // 查询某个大攻略下的攻略分类
    @GetMapping("/{strategyId}/catalogs")
    public Object getCatalogsByStrategyId(@PathVariable Long strategyId){
        return strategyCatalogService.list(strategyId);
    }

    // 查询谋篇攻略的文章内容
    @GetMapping("/details/{id}")
    public Object getDetailById(@PathVariable Long id){
        return strategyDetailService.get(id);
    }



}
