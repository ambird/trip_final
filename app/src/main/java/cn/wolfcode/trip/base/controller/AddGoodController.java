package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.UserGood;
import cn.wolfcode.trip.base.service.IUserGoodService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/addGoods")
public class AddGoodController {
    @Autowired
    private IUserGoodService userGoodService;

    /**
     * 用户添加关注
     * 资源：users/{goodId}
     * 动作:POST
     * 参数：
     */
    @PostMapping("/{goodId}")
    public Object addGood(UserGood userGood) {
        userGood.setUser(UserContext.getCurrentUser());
        userGoodService.addGood(userGood);
        return new JSONResult();
    }

    /**
     * 用户取消关注
     * 资源：users/{goodId}
     * 动作:DELETE
     * 参数：
     */
    @DeleteMapping("/{goodId}")
    public Object removeGood(UserGood userGood) {
        userGood.setUser(UserContext.getCurrentUser());
        userGoodService.removeGood(userGood);
        return new JSONResult();
    }

    @GetMapping("/{goodId}/counts")
    public Object getCount(@PathVariable Long goodId) {
        return userGoodService.getCount(goodId);
    }

}
