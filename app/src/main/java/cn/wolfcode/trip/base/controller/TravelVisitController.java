package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.service.ITravelVisitService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//访问量资源控制器
@RestController
@RequestMapping("/travelVisits")
public class TravelVisitController {
    @Autowired
    private ITravelVisitService travelVisitService;
    /**
     * 新增用户访问游记信息
     * 资源   /travelVisits/{travelId}
     * 动作   POST
     * 参数   id=xx&num=xx
     */
    @PostMapping("/{travelId}")
    public Object insertVisits(TravelVisit visit,@PathVariable Long travelId){
        travelVisitService.insert(visit);
        return new JSONResult();
    }

    /**
     * 修改游记访问信息
     * 资源   /travelVisits/{travelId}
     * 动作   PUT
     * 参数
     */
    @PutMapping("/{travelId}")
    public Object updateVisits(@PathVariable Long travelId,TravelVisit visit){
        JSONResult result = new JSONResult();
        travelVisitService.update(visit);
        result.setData(visit);
        return result;
    }

    /**
     * 查询单个游记的访问信息
     * 资源   /travelVisits/{travelId}
     * 动作   GET
     * 参数
     */
    @GetMapping("/{travelId}")
    public Object getVisits(@PathVariable Long travelId){
        return travelVisitService.get(travelId);
    }
}
