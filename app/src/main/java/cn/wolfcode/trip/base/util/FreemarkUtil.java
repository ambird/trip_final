package cn.wolfcode.trip.base.util;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.Writer;

public abstract class FreemarkUtil {
    private static Configuration config;

    static {
        config = new Configuration(Configuration.VERSION_2_3_28);
        //设置内容编码
        config.setDefaultEncoding("UTF-8");
    }

    public static void writeData(String dir, String tempName, Object data, Writer out)
            throws Exception {
        config.setDirectoryForTemplateLoading(new File(dir));
        Template template = config.getTemplate(tempName);
        template.process(data, out);
    }
}
