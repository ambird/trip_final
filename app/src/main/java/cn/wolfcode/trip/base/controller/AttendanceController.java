package cn.wolfcode.trip.base.controller;

// 签到控制器

import cn.wolfcode.trip.base.domain.Attendance;
import cn.wolfcode.trip.base.service.IAttendanceService;
import cn.wolfcode.trip.base.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;

@RestController
@RequestMapping("/attendances")
public class AttendanceController {

    @Autowired
    private IAttendanceService attendanceService;

    @Autowired
    private IUserService userService;



    /*
        查询（修改）当前用户连续登陆天数和积分
        资源:/attendances/{id}
        动作:GET
        参数:
     */
    @GetMapping("/{id}")
    public Object getConDaysById(@PathVariable Long id){
        // 获取attendance对象
        Attendance attendance = attendanceService.selectByPrimaryKey(id);
        
        // 获取该用户的积分数
        Long integral = attendance.getIntegral();

        // 获取该用户的连续签到天数
        int conDays = attendance.getConDays();

        // 获取该用户最近签到的日期
        Date lastAttendDate = attendance.getSignInTime();

        // 最近签到日Calender对象
        Calendar last = Calendar.getInstance();
        last.setTime(lastAttendDate);

        // 当前日Calendar对象
        Calendar now = Calendar.getInstance();
        Date today = new Date();
        now.setTime(today);

        if(last.getTime().getDay() == now.getTime().getDay()){
            // 已经打过卡了
            attendance.setMsg("今日已签到，请不要重复打卡哦！");
        } else {
            // 积分+5
            integral += 5L;
            attendance.setIntegral(integral);
            attendance.setMsg("今日已签到，获得5积分！");
            int a = last.getTime().getDate();
            int b = now.getTime().getDate();
            if(a + 1 == b){
                // 是连续打卡，连续打卡天数+1
                conDays++;
                attendance.setConDays(conDays);
                if(conDays == 4){
                    // 如果第四天签到，则变回第一天
                    conDays = 1;
                    attendance.setConDays(conDays);

                }
                if(conDays == 3){
                    attendance.setMsg("连续签到三天，恭喜获得5+20积分！");
                    // 连续三天签到，额外奖励20积分
                    integral += 20L;
                    attendance.setIntegral(integral);

                }
            } else {
                // 不是连续打卡，连续打卡天数归1
                conDays = 1;
                attendance.setConDays(1);
            }

            // 更新最近打卡时间
            last = now;
            Date signInTime = last.getTime();
            attendance.setSignInTime(signInTime);

            // 更新积分到数据库
            userService.updateIntegral(attendance.getUser_id(),integral);

            // 更新该用户最近打卡时间到数据库
            attendanceService.updateLastAttendDate(attendance.getId(), signInTime);

            // 更新用户连续签到天数到数据库
            attendanceService.updateConDays(attendance.getId(), conDays);
        }

        // 返回封装好的attendance数据
        return attendance;
    }



}
