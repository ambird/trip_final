package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.query.NewsQueryObject;
import cn.wolfcode.trip.base.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private INewsService newsService;

    /**
     * 查询头条日报
     * 资源:/news
     * 动作:GET
     * 参数:state=3
     */
    @GetMapping
    public Object queryNews(NewsQueryObject qo){
        return newsService.query(qo);
    }

    /**
     * 根据日报的id查询日报的内容
     * 资源:/news/{id}
     * 动作:GET
     * 参数:
     */
    @GetMapping("/{id}")
    public Object getContentById(@PathVariable Long id){
        return newsService.get(id);
    }

}
