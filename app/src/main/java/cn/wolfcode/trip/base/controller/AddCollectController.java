package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.UserCollect;
import cn.wolfcode.trip.base.service.IUserCollectService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/addCollects")
public class AddCollectController {
    @Autowired
    private IUserCollectService userCollectService;

    /**
     * 用户添加收藏
     * 资源：users/{collectId}
     * 动作:POST
     * 参数：
     */
    @PostMapping("/{collectId}")
    public Object addCollect(UserCollect userCollect) {
        userCollect.setUser(UserContext.getCurrentUser());
        userCollectService.addCollect(userCollect);
        return new JSONResult();
    }

    /**
     * 用户取消收藏
     * 资源：users/{collectId}
     * 动作:DELETE
     * 参数：
     */
    @DeleteMapping("/{collectId}")
    public Object removeCollect(UserCollect userCollect) {
        userCollect.setUser(UserContext.getCurrentUser());
        userCollectService.removeCollect(userCollect);
        return new JSONResult();
    }

    @GetMapping("/{collectId}/counts")
    public Object getCount(@PathVariable Long collectId) {
        return userCollectService.getCount(collectId);
    }

}
