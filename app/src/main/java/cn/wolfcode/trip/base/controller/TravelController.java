package cn.wolfcode.trip.base.controller;


import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.service.ITravelVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// 游记资源控制器
@RestController
@RequestMapping("/travels")
public class TravelController {


    @Autowired
    private ITravelService travelService;

    @Autowired
    private ITravelCommendService travelCommendService;
    @Autowired
    private ITravelVisitService travelVisitService;


    /*
    根据游记的id查询游记,用来回显某篇游记的内容
    资源：/travels/{id}
    动作:GET
    参数：
    */
    @GetMapping("/{id}")
    public Object getTravelById(@PathVariable long id){
        return travelService.get(id);
    }

    /*
    获取所有公开发布的游记
    资源：/travels
    动作:get
    参数：isPublic=true&state=2
    */
    @GetMapping
    public Object queryTravels(TravelQueryObject qo){
        return travelService.queryTravel(qo);
    }

    /**
     * 获取攻略推荐
     * @param type
     * @return
     */
    @GetMapping("/commends")
    public Object listCommend(@RequestParam Integer type){
        return travelCommendService.listCommend(type);
    }

    /**
     *  查询当前游记是否存在访问信息中
     *  资源  /travels/{id}/travelVisit
     *  动作  GET
     *  参数
     */
    @GetMapping("/{id}/travelVisit")
    public Object getVitis(@PathVariable Long id){
        return travelVisitService.get(id);
    }

    /**
     * 查询全部符合最低访问量条件的访问游记信息
     */
    @GetMapping("/travelVisit")
    public List<TravelVisit> getVisitsList(){
        return travelService.selectVisits();
    }
}
