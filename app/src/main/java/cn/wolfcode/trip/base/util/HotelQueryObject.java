package cn.wolfcode.trip.base.util;

import cn.wolfcode.trip.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQueryObject extends QueryObject {

    private Integer state = -1;
    private Long regionId=null;
}
