package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/images")
public class ImageController {


    @PostMapping
    public Object upload(@RequestParam MultipartFile pic){
        Map<String, Object> map = new HashMap<>();
        if(pic.getContentType().startsWith("image") && pic.getSize() > 0){
            // 把上传的图片保存到本地图库
            String imgUrl = UploadUtil.upload(pic);
            map.put("status",1);
            map.put("url",imgUrl);
        } else {
            map.put("status",0);
            map.put("msg","请选择图片格式");
        }

        return map;
    }

}
