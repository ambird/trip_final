package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.query.ProductQueryObject;
import cn.wolfcode.trip.base.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 积分商城商品控制器
@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private IProductService productService;

    /*
    获取所有上架的商品
    资源：/products
    动作:get
    参数：state=1
    */
    @GetMapping
    public Object queryProducts(ProductQueryObject qo){
        return productService.queryProduct(qo);
    }

    /*
    获取单个商品
    资源：/products/{id}
    动作:get
    参数：
    */
    @GetMapping("/{id}")
    public Object queryProductById(@PathVariable Long id){
        return productService.get(id);
    }

}
