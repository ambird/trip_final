package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.StrategyVisit;
import cn.wolfcode.trip.base.service.IStrategyVisitService;
import cn.wolfcode.trip.base.service.IStrategyVisitService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//访问量资源控制器
@RestController
@RequestMapping("/strategyVisits")
public class StrategyVisitController {
    @Autowired
    private IStrategyVisitService VisitService;
    /**
     * 新增访问攻略信息
     * 资源   /strategyVisits/{strategyId}
     * 动作   POST
     * 参数   id=xx&num=xx
     */
    @PostMapping("/{strategyId}")
    public Object insertVisits(StrategyVisit visit,@PathVariable Long strategyId){
        VisitService.insert(visit);
        return new JSONResult();
    }

    /**
     * 修改攻略访问信息
     * 资源   /strategyVisits/{strategyId}
     * 动作   PUT
     * 参数
     */
    @PutMapping("/{strategyId}")
    public Object updateVisits(@PathVariable Long strategyId,StrategyVisit visit){
        JSONResult result = new JSONResult();
        VisitService.update(visit);
        result.setData(visit);
        return result;
    }

    /**
     * 查询单个攻略的访问信息
     * 资源   /strategyVisits/{strategyId}
     * 动作   GET
     * 参数
     */
    @GetMapping("/{strategyId}")
    public Object getVisits(@PathVariable Long strategyId){
        return VisitService.get(strategyId);
    }
}
