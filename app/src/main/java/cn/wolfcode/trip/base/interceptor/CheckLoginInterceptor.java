package cn.wolfcode.trip.base.interceptor;

import cn.wolfcode.trip.base.controller.UserContext;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckLoginInterceptor extends HandlerInterceptorAdapter {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(UserContext.getCurrentUser() == null){
            response.sendRedirect("/login.html");
            return false;
        }
        return true;
    }
}
