package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.Hotel;
import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.domain.StrategyVisit;
import cn.wolfcode.trip.base.query.StrategyQueryObject;
import cn.wolfcode.trip.base.service.IHotelService;
import cn.wolfcode.trip.base.service.IRegionService;
import cn.wolfcode.trip.base.service.IStrategyService;
import cn.wolfcode.trip.base.service.IStrategyVisitService;
import cn.wolfcode.trip.base.util.FreemarkUtil;
import cn.wolfcode.trip.base.util.HotelQueryObject;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/regions")
public class RegionsController {

    @Autowired
    private IRegionService regionService;

    @Autowired
    private ServletContext ctx;

    @Autowired
    private IStrategyService strategyService;


    @Autowired
    private IHotelService hotelService;

    @Autowired
    private IStrategyVisitService strategyVisitService;



    //获取某个区域下的宾馆信息
    @GetMapping(value = "/{regionId}/hotels", produces = "text/html;charset=UTF-8")
    public void getHotelsInRegion(HttpServletResponse resp, HotelQueryObject qo) throws Exception {
        String dir = ctx.getRealPath("/templateByHotel");
        Map<String, Object> map = new HashMap<>();

        // 1.全部数据
        qo.setState(-1);
        qo.setPageSize(2);
        PageInfo<Hotel> pageInfo = hotelService.query(qo);
        // 如果是翻页操作则直接返回数据即可，不需要再次出现精品推荐
        if(qo.getCurrentPage() != 1){
            map.put("info",pageInfo);
            FreemarkUtil.writeData(dir,"classHotel.ftl",map,resp.getWriter());
            return;
        }
        // 2.精品推荐
        qo.setState(2);
        qo.setPageSize(0);
        List<Hotel> commends = hotelService.query(qo).getList();

        map.put("commends",commends);
        map.put("info",pageInfo);
        map.put("id",qo.getRegionId()); // 获取当前区域的id

        FreemarkUtil.writeData(dir,"HotelContent.ftl",map,resp.getWriter());
    }


    @GetMapping
    public Object list(Integer state){
        return regionService.list(state);
    }

    // 获取某个区域推荐的攻略
    @GetMapping(value = "/{regionId}/strategies", produces = "text/html;charset=UTF-8")
    public void queryStrategies(StrategyQueryObject qo, HttpServletResponse resp) throws Exception{
        String dir = ctx.getRealPath("/template");
        Map<String, Object> map = new HashMap<>();

        // 1.全部数据
        qo.setState(-1L);
        qo.setPageSize(2);
        PageInfo<Strategy> pageInfo = strategyService.query(qo);
        // 如果是翻页操作则直接返回数据即可，不需要再次出现精品推荐
        if(qo.getCurrentPage() != 1){
            map.put("info",pageInfo);
            FreemarkUtil.writeData(dir,"classify.ftl",map,resp.getWriter());
            return;
        }
        // 2.精品推荐
        qo.setState(2L);
        qo.setPageSize(0);
        List<Strategy> commends = strategyService.query(qo).getList();
        // 查询访问量
        for (Strategy commend : commends) {
            StrategyVisit visit = strategyVisitService.get(commend.getId());
            commend.setVisit(visit.getNum());
        }

        map.put("commends",commends);
        map.put("info",pageInfo);
        map.put("id",qo.getRegionId()); // 获取当前区域的id

        FreemarkUtil.writeData(dir,"strategyContent.ftl",map,resp.getWriter());
    }
}
