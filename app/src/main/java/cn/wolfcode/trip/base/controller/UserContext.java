package cn.wolfcode.trip.base.controller;

import cn.wolfcode.trip.base.domain.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

public abstract class UserContext {
    private static final String USER_IN_SESSION_KEY = "USER_IN_SESSION";

    private static HttpSession getSession() {
        return ((ServletRequestAttributes)(RequestContextHolder.getRequestAttributes()))
                .getRequest().getSession();
    }

    public static void setCurrentUser(User user) {

        getSession().setAttribute(USER_IN_SESSION_KEY,user);
    }

    public static User getCurrentUser() {
        return (User) getSession().getAttribute(USER_IN_SESSION_KEY);
    }
}
