<div class="tab-pane fade" id="pills-${id}" data-pages="${info.pages}">
    <div class="container">
        <h6>推荐酒店</h6>
        <div class="row hot">
            <#list commends as ced>
                <div class="col">
                    <a href="/hotelContent.html?id=${ced.id}">
                        <img src="${ced.coverUrl}">
                    </a>
                </div>
            </#list>
        </div>
    </div>
    <hr>
    <div class="container">
        <h6>全部酒店</h6>
        <div class="row classify">
            <#include "classHotel.ftl"/>
        </div>
    </div>
</div>