<#list info.list as ced>
    <div class="col-6 mb">
        <a href="/hotelContent.html?id=${ced.id}">
            <img class="float-left " src="${ced.coverUrl}">
            <div class="classify-text">
                <span>${ced.title}</span>
            </div>
        </a>
    </div>
</#list>