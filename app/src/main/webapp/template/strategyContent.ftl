<div class="tab-pane fade" id="pills-${id}" data-pages="${info.pages}">
    <div class="container">
        <h6>当季推荐</h6>
        <div class="row hot">
            <#list commends as ced>
                <div class="col read" data-did="${ced.id}">
                    <a href="/strategyCatalogs.html?strategyId=${ced.id}">
                        <img src="${ced.coverUrl}">
                        <p>${ced.visit}次浏览</p>
                    </a>
                </div>
            </#list>
        </div>
    </div>
    <hr>
    <div class="container">
        <h6>全部攻略</h6>
        <div class="row classify">
            <#include "classify.ftl"/>
        </div>
    </div>
</div>
