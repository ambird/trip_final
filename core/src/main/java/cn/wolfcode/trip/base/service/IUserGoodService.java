package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.UserGood;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

public interface IUserGoodService {
    /**
     * 生成点赞id
     */
    Long autoUserGoodId();

    void addGood(UserGood userGood);

    /**
     * 检查用户是否已点赞
     * @return 用户点赞信息
     */
    UserGood checkAddGood(Long userId, Long goodId);

    /**
     * 取消赞
     * @param userGood 用户点赞信息
     */
    void removeGood(UserGood userGood);

    /**
     * 查询点赞数量
     * @param goodId 点赞内容Id
     * @return 点赞数量
     */
    int getCount(Long goodId);

    /**
     * 查询用户的点赞信息(分页)
     * @return
     */
    PageInfo<UserGood> userAddGoodMsg(Long id, QueryObject qo);
}
