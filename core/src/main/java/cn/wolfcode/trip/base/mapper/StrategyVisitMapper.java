package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.StrategyVisit;
import java.util.List;

public interface StrategyVisitMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StrategyVisit record);

    StrategyVisit selectByPrimaryKey(Long id);

    List<StrategyVisit> selectAll();

    int updateByPrimaryKey(StrategyVisit record);
}