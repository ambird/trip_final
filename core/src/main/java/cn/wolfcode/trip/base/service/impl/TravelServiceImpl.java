package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.mapper.CommentMapper;
import cn.wolfcode.trip.base.mapper.TravelContentMapper;
import cn.wolfcode.trip.base.mapper.TravelMapper;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import cn.wolfcode.trip.base.service.ICommentService;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.service.IUserCollectService;
import cn.wolfcode.trip.base.service.IUserGoodService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TravelServiceImpl implements ITravelService {

    @Autowired
    private TravelMapper travelMapper;
    @Autowired
    private TravelContentMapper travelContentMapper;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IUserGoodService userGoodService;
    @Autowired
    private IUserCollectService userCollectService;
    @Autowired
    private CommentMapper commentMapper;

    public PageInfo<Travel> queryTravel(TravelQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Travel> list = travelMapper.selectForList(qo);
        // 查询游记的评论量
        if (list.size() >0) {
            for (Travel travel : list) {
                int count = commentMapper.getCount(travel.getCommentId());
                travel.setComments(count);
            }
        }
        return new PageInfo<>(list);
    }


    // 新增、更新游记
    public void saveOrUpdate(Travel entity) {
        if(entity.getId() == null){
            // 新增
            // 填充创建时间/更新时间
            Date d = new Date();
            entity.setCreateTime(d);
            entity.setLastUpdateTime(d);
            // 获取游记所属的评论id
            Long commentId = commentService.autoCommentId();
            entity.setCommentId(commentId);
            // 获取游记所属的点赞id
            Long goodId = userGoodService.autoUserGoodId();
            entity.setGoodId(goodId);

            // 获取所属的收藏id
            Long collectId = userCollectService.autoUserCollectId();
            entity.setCollectId(collectId);

            travelMapper.insert(entity);

            // 同步游记的内容
            TravelContent travelContent = entity.getTravelContent();
            // 主键关联
            travelContent.setId(entity.getId());
            travelContentMapper.insert(travelContent);
        } else {
            // 更新
            // 设置最后跟新时间
            entity.setLastUpdateTime(new Date());
            travelMapper.updateByPrimaryKey(entity);
            // 同步游记的内容
            TravelContent travelContent = entity.getTravelContent();
            // 主键关联
            travelContent.setId(entity.getId());
            // 更新游记内容
            travelContentMapper.updateByPrimaryKey(travelContent);
        }
    }

    // 根据游记的id查询游记
    public Travel get(long id) {
        Travel travel = travelMapper.selectByPrimaryKey(id);
        // 根据游记的id查询游记内容对象，设置到游记中（主键关联）
        TravelContent travelContent = travelContentMapper.selectByPrimaryKey(id);
        travel.setTravelContent(travelContent);
        return travel;
    }

    // 获得游记到正文内容
    public String getContent(Long id) {
        return travelContentMapper.selectByPrimaryKey(id).getContent();
    }

    // 提交、拒绝游记
    public void changeState(Long id, Integer state) {
        if(state == Travel.STATE_REFUSE){
            // 拒绝操作
            travelMapper.updateState(id, state, null);
        } else {
            // 发布操作
            travelMapper.updateState(id, state, new Date());
        }
    }

    //查询所有满足最低访问量的游记访问信息
    @Override
    public List<TravelVisit> selectVisits() {
        return travelMapper.selectVisits();
    }

    public Integer getTravelCountById(Long authorId) {
        return travelMapper.getTravelCount(authorId);
    }

}
