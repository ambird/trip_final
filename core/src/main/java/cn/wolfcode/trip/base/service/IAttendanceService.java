package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Attendance;

import java.util.Date;
import java.util.List;

public interface IAttendanceService {

    int insert(Attendance record);

    int updateByPrimaryKey(Attendance record);

    List<Attendance> selectAll();

    Attendance selectByPrimaryKey(Long id);

//    Date getLastAttendDateById(Long id);

    /**
     * 更新最近打卡时间
     * @param signInTime
     */
    void updateLastAttendDate(Long id, Date signInTime);

    /**
     * 更新连续签到天数
     * @param id
     * @param conDays
     */
    void updateConDays(Long id, int conDays);
}
