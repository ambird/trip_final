package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StrategyQueryObject extends QueryObject {
    private Long state = -1L; // 大攻略的状态
    private Long regionId;
}
