package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Hotel;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IHotelService {


    List<Hotel> listHotel(Integer state);

    Hotel getHotels(Long id);

    PageInfo getAlls(QueryObject qo);

    List<Hotel> getAllHotels(QueryObject qo);


    //返回所有推介的数据
    PageInfo<Hotel> query(QueryObject qo);

    PageInfo<Hotel>  queryHotelInAdmin(QueryObject qo);

    void saveOrUpdate(Hotel entity);

    List<Hotel> queryHotels();
}
