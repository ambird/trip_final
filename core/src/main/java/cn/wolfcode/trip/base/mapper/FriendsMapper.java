package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Friends;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FriendsMapper {
    int deleteByPrimaryKey(@Param("userId") Long userId, @Param("toUserId") Long toUserId);

    int insert(Friends record);

    Friends selectByPrimaryKey(Long id);

    List<Friends> selectFans(QueryObject qo);

    List<Friends> selectFollow(QueryObject qo);

    Friends selectFollowOrFans(@Param("userId") Long userId, @Param("toUserId") Long toUserId);
}