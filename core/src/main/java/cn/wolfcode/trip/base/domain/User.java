package cn.wolfcode.trip.base.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseDomain{

    public static final Integer GENDER_NONE = 0; // 保密
    public static final Integer GENDER_MALE = 1; // 男
    public static final Integer GENDER_FEMALE = 2; // 女


    private String email; // 邮箱

    private String nickName; // 昵称

    private String password; // 密码

    private String place; // 地方

    private String headImgUrl; // 头像

    private Integer gender = GENDER_NONE; // 性别

    private String coverImgUrl; // 封面

    private String sign; // 签名

    private Long integral; // 打卡获得的商城积分



    private PersonalInterface personalInterface;

    // 返回gender
    public String getGenderName(){
        String msg = "保密";
        if(gender == GENDER_MALE){
            msg = "男";
        } else if (gender == GENDER_FEMALE){
            msg = "女";
        }
        return msg;
    }

}