package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.SystemMsg;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

/**
 * 系统信息服务接口
 */
public interface ISystemMsgService {

    /**
     * 根据id查看我的系统信息
     * @param id 用户Id
     * @return 我的信息
     */
    PageInfo<SystemMsg> selectUserSystemMsg(QueryObject qo, Long id);

    /**
     * 分页查询
     * @param qo
     * @return
     */
    PageInfo<SystemMsg> query(QueryObject qo);

    /**
     * 查询内容
     * @param id
     * @return
     */
    String selectContentById(Long id);

    /**
     * 保存发布的信息
     * @param entity
     */
    void save(SystemMsg entity);
}
