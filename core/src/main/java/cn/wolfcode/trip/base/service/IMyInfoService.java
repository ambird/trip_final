package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.MyInfo;

public interface IMyInfoService {

    /**
     * 根据id查看我的信息
     * @param id 用户Id
     * @return 我的信息
     */
    MyInfo getInfoByUserId(Long id);
}
