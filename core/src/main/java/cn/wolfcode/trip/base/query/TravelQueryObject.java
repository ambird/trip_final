package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelQueryObject extends QueryObject{

    private String authorId; // 作者id

    private Integer state; // 游记状态，这里不给默认值，controller里会给默认值

    private Boolean isPublic; // 是否公开

    private Long travelId; // 游记的id
}
