package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 用户点赞信息
 */
@Getter
@Setter
public class UserGood extends BaseDomain {
    private Long goodId; // 点赞内容id

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;  // 点赞时间

    private User user;   // 点赞用户

    private Travel travel;

    private Strategy strategy;

    private News news;

    private Hotel hotel;

    private Object allAddGoodMsg;

}