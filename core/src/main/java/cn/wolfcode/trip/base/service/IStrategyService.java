package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IStrategyService {

    void saveOrUpdate(Strategy entity);

    PageInfo<Strategy> query(QueryObject qo);

    List<Strategy> list();

    Strategy get(Long id);
}
