package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.domain.UserCollect;
import cn.wolfcode.trip.base.mapper.MyInfoMapper;
import cn.wolfcode.trip.base.mapper.UserCollectMapper;
import cn.wolfcode.trip.base.service.IUserCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class UserCollectServiceImpl implements IUserCollectService{

    @Autowired
    private UserCollectMapper userCollectMapper;
    @Autowired
    private MyInfoMapper infoMapper;

    public List<UserCollect> getCollectsByType(Long userId, String type) {
        return userCollectMapper.getCollectsByType(userId,type);
    }

    public int getCollectsCountByType(Long userId, String type) {
        int count = userCollectMapper.getCollectsCountByType(userId, type);
        return count;

    }

    public Long autoUserCollectId() {
        User user = new User();
        userCollectMapper.autoUserCollectId(user);
        return user.getId();
    }

    public void addCollect(UserCollect userCollect) {
        // 点赞内容id
        userCollect.setCollectId(userCollect.getCollectId());
        userCollect.setTime(new Date());
        userCollectMapper.insert(userCollect);
    }

    public UserCollect checkAddCollect(Long userId, Long collectId) {
        UserCollect userCollect = userCollectMapper.selectCollectMsgByUserIdAndCollectId(userId, collectId);
        if (userCollect != null) {
            throw new RuntimeException("已收藏");
        }
        return userCollect;
    }

    public void removeCollect(UserCollect userCollect) {
        // 取消收藏内容id
        userCollect.setCollectId(userCollect.getCollectId());
        userCollectMapper.deleteCollect(userCollect);
    }

    public int getCount(Long collectId) {
        return userCollectMapper.selectCountCollect(collectId);
    }
}
