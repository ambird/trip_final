package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties("handler")
public class Travel extends BaseDomain{

    public static final Integer STATE_REFUSE = -1; //拒绝
    public static final Integer STATE_TEMP = 0; //草稿
    public static final Integer STATE_WAIT = 1; //待审核
    public static final Integer STATE_RELEASE = 2; //审核通过

    private String title;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date travelTime;
    private String perExpends;
    private Integer days;
    private Integer person = 0;
    private User author;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date releaseTime;
    // isPublic 没有勾，不要null,而是false 所以默认给个false
    private Boolean isPublic = false;
    private Region place;
    private String coverUrl;
    private TravelCommend commend;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastUpdateTime; // 最后更新时间
    private Integer state = STATE_TEMP; // 默认是草稿状态
    // 游记内容，一对一，使用主键关联
    private TravelContent travelContent;

    //访问信息,一对一
    private  TravelVisit visit;

    private Long commentId; // 所属的评论id

    private Long goodId; // 所属的点赞id

    private Long collectId; // 所属的收藏id

    private int goods; // 点赞数

    private int comments; // 评论数

    // 封装转化state为文字，以用来回显
    public String getStateName(){
        String msg = "拒绝";
        if(state == STATE_TEMP){
            msg = "草稿";
        } else if(state == STATE_WAIT){
            msg = "待审核";
        } else if(state == STATE_RELEASE){
            msg = "审核通过";
        }
        return msg;
    }

    // 和谁一起去，转化为文字
    public String getPersonName(){
        String msg = "一个人的旅行";
        if(person == 2){
            msg = "和父母";
        } else if(person == 3){
            msg = "和朋友";
        }else if(person == 4){
            msg = "和同事";
        }else if(person == 5){
            msg = "和爱人";
        }else if(person == 6){
            msg = "和驴友";
        }

        return msg;
    }

    public String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("title",title);
        map.put("coverUrl",coverUrl);
        return JSONUtil.toJSONString(map);
    }

}