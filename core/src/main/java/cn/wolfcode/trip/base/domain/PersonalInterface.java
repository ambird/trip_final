package cn.wolfcode.trip.base.domain;

import lombok.Data;

@Data
public class PersonalInterface extends BaseDomain {
    private Integer fans;

    private Integer follow;
}