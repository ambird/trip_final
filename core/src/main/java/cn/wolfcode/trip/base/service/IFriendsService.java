package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Friends;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

public interface IFriendsService {
    PageInfo<Friends> selectFans(QueryObject qo);

    PageInfo<Friends> selectFollow(QueryObject qo);

    Friends getfansOrFollow(Long userId, Long toUserId);

    void deleteFansOrFollow(Long userId, Long toUserId);
}
