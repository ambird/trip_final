package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 点评详情
 */
@Getter
@Setter
public class Comment extends BaseDomain{
    private User commentator;   // 评论人

    private User toCommentator; // 被评论人

    private Integer good;   // 点赞次数

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;  // 评论时间

    private String content; // 内容

    //冗余字段
    private News news;//日报

    private Strategy strategy;//攻略

    private Travel travel;//游记

    private Hotel hotel;//酒店

    private Object allComments;
}