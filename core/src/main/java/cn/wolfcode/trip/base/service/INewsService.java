package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.News;
import cn.wolfcode.trip.base.query.NewsQueryObject;
import com.github.pagehelper.PageInfo;

public interface INewsService {
    /**
     * 日报的分页查询
     * @param qo
     * @return
     */
    PageInfo<News> query(NewsQueryObject qo);


    void saveOrUpdate(News entity);

    /**
     * 获取日报的内容
     * @param id
     * @return
     */
    String getContent(Long id);

    /**
     * 把日报的状态设置为下架
     * @param id
     * @param state
     */
    void changeState(Long id, Integer state);


    News get(long id);
}
