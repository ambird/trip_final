package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.UserCollect;
import java.util.List;
public interface IUserCollectService {
    /**
     * 生成收藏id
     */
    Long autoUserCollectId();

    void addCollect(UserCollect userCollect);

    /**
     * 检查用户是否已收藏
     * @return 用户收藏信息
     */
    UserCollect checkAddCollect(Long userId, Long collectId);

    /**
     * 取消赞
     * @param userCollect 用户收藏信息
     */
    void removeCollect(UserCollect userCollect);

    /**
     * 查询收藏数量
     * @param collectId 收藏内容Id
     * @return 收藏数量
     */
    int getCount(Long collectId);




    /**
     * 查找该用户收藏的类型
     * @param userId
     * @param type
     * @return
     */
    List<UserCollect> getCollectsByType(Long userId, String type);

    /**
     * 查询某个用户在某个类型的收藏数量
     * @param userId
     * @param type
     * @return
     */
    int getCollectsCountByType(Long userId, String type);
}
