package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Hotel;
import cn.wolfcode.trip.base.mapper.HotelContentMapper;
import cn.wolfcode.trip.base.mapper.HotelMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ICommentService;
import cn.wolfcode.trip.base.service.IHotelService;
import cn.wolfcode.trip.base.service.IUserCollectService;
import cn.wolfcode.trip.base.service.IUserGoodService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelServiceImpl implements IHotelService {

    //注入对应的mapper
    @Autowired
    private HotelMapper hotelMapper;

    //注入内容的信息
    @Autowired
    private HotelContentMapper hotelContentMapper;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IUserGoodService userGoodService;
    @Autowired
    private IUserCollectService userCollectService;

    @Override
    public List<Hotel> listHotel(Integer state) {
        return hotelMapper.selectForList(state);
    }

    //查询酒店的信息
    @Override
    public Hotel getHotels(Long id) {


        return hotelMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageInfo getAlls(QueryObject qo) {

        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo(hotelMapper.selectInAdmin(qo));
    }

    @Override
    public List<Hotel> getAllHotels(QueryObject qo) {
        return hotelMapper.getHotelsByState(qo);
    }

    @Override
    public PageInfo<Hotel> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo<>(hotelMapper.getHotelsByState(qo));
    }

    @Override
    public PageInfo<Hotel> queryHotelInAdmin(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        return new PageInfo<>(hotelMapper.selectInAdmin(qo));
    }


    //保存或者更新的操作
    @Override
    public void saveOrUpdate(Hotel entity) {
        if (entity.getId() == null) {
            // 获取所属的评论id
            Long commentId = commentService.autoCommentId();
            entity.setCommentId(commentId);

            // 获取所属的点赞id
            Long goodId = userGoodService.autoUserGoodId();
            entity.setGoodId(goodId);

            // 获取所属的收藏id
            Long collectId = userCollectService.autoUserCollectId();
            entity.setCollectId(collectId);

            hotelMapper.insert(entity);
        }
        hotelMapper.updateByPrimaryKey(entity);
    }

    @Override
    public List<Hotel> queryHotels() {
        return hotelMapper.selectAll();
    }

}
