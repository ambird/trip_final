package cn.wolfcode.trip.base.util;

import lombok.Getter;
import lombok.Setter;

// json格式封装结果类
@Getter
public class JSONResult {
    // 结果对象封装到data中
    @Setter
    private Object data;
    private boolean success = true;
    private String msg;

    public JSONResult mark(String msg){
        this.success = false;
        this.msg = msg;
        return this;
    }
}
