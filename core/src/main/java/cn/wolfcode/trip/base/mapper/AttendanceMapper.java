package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Attendance;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface AttendanceMapper {

    int insert(Attendance record);

    Attendance selectByPrimaryKey(Long id);

    List<Attendance> selectAll();

    int updateByPrimaryKey(Attendance record);

    Date getLastAttendDateById(Long id);

//    Long getIntegralById(Long id);

    void updateLastAttendDate(@Param("id") Long id, @Param("signInTime") Date signInTime);

    void updateConDays(@Param("id") Long id, @Param("conDays") int conDays);
}