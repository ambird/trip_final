package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IStrategyCatalogService {

    void saveOrUpdate(StrategyCatalog entity);

    PageInfo<StrategyCatalog> query(QueryObject qo);

    List<StrategyCatalog> list(Long strategyId);

}
