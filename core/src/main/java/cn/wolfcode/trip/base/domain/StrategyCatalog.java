package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties("handler")
public class StrategyCatalog extends BaseDomain{

    private String name; // 攻略分类名称

    private Integer sequence; // 序号

    private boolean state; // 默认为false

    // private Strategy strategy; // 大攻略
    // 使用冗余字段
    private Long strategyId;
    private String strategyTitle;

    private List<StrategyDetail> details = new ArrayList<>(); // 攻略文章

    // 状态要翻译
    public String getStateName(){
        String msg = "禁用";
        if(state){
            msg = "启用";
        }
        return msg;
    }

    // 封装json数据
    public String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("sequence",sequence);
        map.put("state",state);
        map.put("strategyId",strategyId);

        return JSONUtil.toJSONString(map);
    }
}