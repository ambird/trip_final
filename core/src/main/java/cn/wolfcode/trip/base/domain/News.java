package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
public class News extends BaseDomain{
    public static final Integer STATE_HEAD_NEWS = 3; //头条
    public static final Integer STATE_RELEASE = 2; //已发布
    public static final Integer STATE_NOT_RELEASE = 1; //未发布
    public static final Integer STATE_SOLD_OUT = 0; //下架

    public static final Integer STATE_COLLECT = 1; //收藏
    public static final Integer STATE_NOT_COLLECT = 1; //不收藏

    private String title;//日报标题

    private String subTitle;//日报副标题

    private String coverUrl;//日报封面

    private Integer good;//点赞数

    private Long goodId; // 所属点赞id

    private Long collectId; // 所属收藏id

    private int goods;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date releaseTime;//发布时间

    private Integer collect;//收藏  0表示收藏 , 1表示不收藏

    private Integer state;//状态  0表示已发布,1表示未发布,2表示下架,3表示头条

    private Long commentId;//评论

    private NewsContent newsContent;//日报的内容

    public String getStateName(){
        String msg = "已发布";
        if (state == STATE_NOT_RELEASE){
            msg = "未发布";
        }else if (state == STATE_SOLD_OUT){
            msg = "下架";
        }else if (state == STATE_HEAD_NEWS){
            msg = "头条日报";
        }
        return msg;
    }

    public String getCollectName(){
        String msg = "收藏";
        if (collect == STATE_NOT_COLLECT){
            msg = "不收藏";
        }
        return msg;
    }

    public String getJson(){
        Map map = new HashMap();
        map.put("id",id);
        map.put("title",title);
        map.put("subTitle",subTitle);
        map.put("coverUrl",coverUrl);
        map.put("good",good);
        map.put("collect",collect);
        map.put("state",state);
            map.put("commentId",commentId);
        return JSONUtil.toJSONString(map);
    }

}