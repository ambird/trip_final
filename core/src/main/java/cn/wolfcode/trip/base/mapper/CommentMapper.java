package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Comment record);

    Comment selectByPrimaryKey(Long id);

    List<Comment> selectAll();

    int updateByPrimaryKey(Comment record);

    List<Comment> listCommentById(Long id);

    //List<Comment> selectCommentAllByUserId(Long commentatorId);

    int autoCommentId(Comment comment);

    List<Comment> selectUserCommentByType(@Param("userId") Long userId, @Param("type") String type);

    int selectCount(@Param("userId") Long userId, @Param("type") String type);

    List<Comment> selectCommentAllByUserId(Long userId);

    Integer getCommentCountByUserId(Long userId);

    int getCount(Long commentId);
}