package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Attendance;
import cn.wolfcode.trip.base.mapper.AttendanceMapper;
import cn.wolfcode.trip.base.service.IAttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AttendanceServiceImpl implements IAttendanceService {
    @Autowired
    private AttendanceMapper attendanceMapper;

    public int insert(Attendance record) {
        return attendanceMapper.insert(record);
    }

    public int updateByPrimaryKey(Attendance record) {
        return attendanceMapper.updateByPrimaryKey(record);
    }

    public List<Attendance> selectAll() {
        return attendanceMapper.selectAll();
    }

    public Attendance selectByPrimaryKey(Long id) {
        return attendanceMapper.selectByPrimaryKey(id);
    }


    // 获取user最近的一次打卡日期
//    public Date getLastAttendDateById(Long id) {
//        return attendanceMapper.getLastAttendDateById(id);
//    }

    // 更新最近一次打卡日期
    public void updateLastAttendDate(Long id, Date signInTime) {
        attendanceMapper.updateLastAttendDate(id, signInTime);
    }

    // 更新连续签到天数
    public void updateConDays(Long id, int conDays) {
        attendanceMapper.updateConDays(id, conDays);
    }


}
