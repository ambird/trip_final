package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.News;
import cn.wolfcode.trip.base.domain.NewsContent;
import cn.wolfcode.trip.base.mapper.CommentMapper;
import cn.wolfcode.trip.base.mapper.NewsContentMapper;
import cn.wolfcode.trip.base.mapper.NewsMapper;
import cn.wolfcode.trip.base.query.NewsQueryObject;
import cn.wolfcode.trip.base.service.ICommentService;
import cn.wolfcode.trip.base.service.INewsService;
import cn.wolfcode.trip.base.service.IUserCollectService;
import cn.wolfcode.trip.base.service.IUserGoodService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class NewsServiceImpl implements INewsService {

    @Autowired
    private NewsMapper newsMapper;
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private NewsContentMapper newsContentMapper;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IUserGoodService userGoodService;
    @Autowired
    private IUserCollectService userCollectService;

    public PageInfo<News> query(NewsQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<News> list = newsMapper.selectForList(qo);
        // 查询日报的点赞量
        if (list.size() > 0) {
            for (News news : list) {
                int count = userGoodService.getCount(news.getGoodId());
                news.setGoods(count);
            }
        }
        return new PageInfo<>(list);
    }

    public void saveOrUpdate(News entity) {
        //设置发布时间
        if (entity.getState() == News.STATE_RELEASE) {
            entity.setReleaseTime(new Date());
        }
        //文章内容主键关联
        NewsContent newsContent = entity.getNewsContent();
        if (entity.getId() == null) {

            //新增状态默认为未发布
            entity.setState(1);
            //新增点赞数默认为0
            entity.setGood(0);
            //新增收藏默认为未收藏
            entity.setCollect(1);
            //创建发布时间
            entity.setCreateTime(new Date());

            // 获取所属的评论id
            Long commentId = commentService.autoCommentId();
            entity.setCommentId(commentId);
            // 获取所属的点赞id
            Long goodId = userGoodService.autoUserGoodId();
            entity.setGoodId(goodId);

            // 获取所属的收藏id
            Long collectId = userCollectService.autoUserCollectId();
            entity.setCollectId(collectId);

            newsMapper.insert(entity);
            //保存内容
            newsContent.setId(entity.getId());
            newsContentMapper.insert(newsContent);
        } else {
            newsMapper.updateByPrimaryKey(entity);
            //保存内容
            newsContent.setId(entity.getId());
            newsContentMapper.updateByPrimaryKey(newsContent);
        }
    }

    public String getContent(Long id) {
        return newsContentMapper.getContent(id).getContent();
    }

    //发布或下架
    public void changeState(Long id, Integer state) {
        if (state == News.STATE_SOLD_OUT) {
            //下架
            newsMapper.updateState(id, state, null);
        } else if (state == News.STATE_HEAD_NEWS) {
            //把之前头条的状态设置为已发布
            newsMapper.updateOldState(state);
            //头条
            newsMapper.updateState(id,state,null);
        } else {
            //发布
            newsMapper.updateState(id, state, new Date());
        }
    }

    public News get(long id) {
        News news = newsMapper.selectByPrimaryKey(id);
        NewsContent newsContent = newsContentMapper.selectByPrimaryKey(id);
        news.setNewsContent(newsContent);
        return news;
    }
}
