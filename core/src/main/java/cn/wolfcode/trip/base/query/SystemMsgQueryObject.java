package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SystemMsgQueryObject extends QueryObject{
    private Integer state;
}
