package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyVisit;
import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.mapper.StrategyVisitMapper;
import cn.wolfcode.trip.base.mapper.TravelVisitMapper;
import cn.wolfcode.trip.base.service.IStrategyVisitService;
import cn.wolfcode.trip.base.service.ITravelVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyVisitServiceImpl implements IStrategyVisitService {
    @Autowired
    private StrategyVisitMapper visitMapper;

    @Override
    public void insert(StrategyVisit visit) {
        visitMapper.insert(visit);
    }

    @Override
    public void update(StrategyVisit visit) {
        visitMapper.updateByPrimaryKey(visit);
    }

    @Override
    public StrategyVisit get(Long travelId) {
        return visitMapper.selectByPrimaryKey(travelId);
    }

}
