package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.HotelContent;

import java.util.List;

public interface HotelContentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(HotelContent record);

    HotelContent selectByPrimaryKey(Long id);

    List<HotelContent> selectAll();

    int updateByPrimaryKey(HotelContent record);
}