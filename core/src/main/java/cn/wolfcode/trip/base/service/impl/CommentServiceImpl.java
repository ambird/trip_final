package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Comment;
import cn.wolfcode.trip.base.domain.MyInfo;
import cn.wolfcode.trip.base.domain.MyInfoTemp;
import cn.wolfcode.trip.base.mapper.CommentMapper;
import cn.wolfcode.trip.base.mapper.MyInfoMapper;
import cn.wolfcode.trip.base.mapper.MyInfoTempMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ICommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements ICommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private MyInfoMapper infoMapper;
    @Autowired
    private MyInfoTempMapper infoTempMapper;

    public void saveComment(Comment comment) {
        comment.setTime(new Date());
        commentMapper.insert(comment);

        // 我的信息-评论信息+1
        MyInfo info = infoMapper.selectInfoByUserId(comment.getCommentator().getId());
        infoMapper.updateInfo(comment.getCommentator().getId(), info.getComment() + 1, "comment");

        // 我的信息临时表-评论消息+1
        MyInfoTemp infoTemp = infoTempMapper.selectCount("comment", comment.getCommentator().getId());
        infoTempMapper.updataInfo(comment.getCommentator().getId(), infoTemp.getComment() + 1, "comment");
    }

    public PageInfo<Comment> queryCommentById(Long id, QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<Comment> list = commentMapper.listCommentById(id);
        return new PageInfo<>(list);
    }

    public Long autoCommentId() {
        Comment comment = new Comment();
        commentMapper.autoCommentId(comment);
        return comment.getId();
    }



    public List<Comment> getUserCommentByType(Long userId, String type) {
        List<Comment> list = commentMapper.selectUserCommentByType(userId, type);
        return list;
    }

    public int getUserCommentsCountByType(Long userId, String type) {
        int count = commentMapper.selectCount(userId,type);
        return count;
    }

    public PageInfo<Comment> selectCommentAllByUserId(Long userId, QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Comment> list = commentMapper.selectCommentAllByUserId(userId);
        return new PageInfo<>(list);
    }

    public Integer getCommentCountByUserId(Long userId) {
        return commentMapper.getCommentCountByUserId(userId);
    }



    public PageInfo<Comment> userCommentMsg(Long id, QueryObject qo) {
        // 查出个人临时表中的评论数量
        MyInfoTemp infoTemp = infoTempMapper.selectCount("comment", id);
        // 当点赞数量不为0时查询所有的临时点赞数
        if (infoTemp.getComment() != 0) {
                if (infoTemp.getComment() > 20) {
                    PageHelper.startPage(qo.getCurrentPage(), 20, qo.getOrderBy());
                } else {
                    PageHelper.startPage(qo.getCurrentPage(), infoTemp.getComment(), qo.getOrderBy());
                }
                List<Comment> list = commentMapper.selectCommentAllByUserId(id);
                if (list.size() > 0) {
                // 获取评论信息
                for (Comment comment : list) {
                    if (comment.getTravel() != null) {
                        comment.setAllComments(comment.getTravel());
                    } else if (comment.getStrategy() != null) {
                        comment.setAllComments(comment.getStrategy());
                    } else if (comment.getNews() != null) {
                        comment.setAllComments(comment.getNews());
                    } else if (comment.getHotel() != null) {
                        comment.setAllComments(comment.getHotel());
                    }
                }
            }

            // 清空个人界面评论信息
            infoMapper.updateInfo(id, 0, "comment");

            return  new PageInfo<>(list);
        }
        return null;

    }
}
