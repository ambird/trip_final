package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.mapper.MyInfoTempMapper;
import cn.wolfcode.trip.base.service.IMyInfoTempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyInfoTempServiceImpl implements IMyInfoTempService {
    @Autowired
    private MyInfoTempMapper infoTempMapper;

    public void emptyTempMsg(Long id, String msgType) {
        infoTempMapper.updateTempMsg(id, msgType);
    }
}
