package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class StrategyDetail extends BaseDomain{

    public static final Integer STATE_RELEASE = 0; //发布
    public static final Integer STATE_TEMP = 1; //草稿

    private String title; // 攻略文章标题

    private Date createTime; // 创建时间

    private Date releaseTime; // 发布时间

    private Integer sequence; // 序号

    private String coverUrl; // 封面

    private Integer state = STATE_TEMP; // 状态

    private StrategyCatalog catalog; // 所属的攻略分类

    private StrategyContent strategyContent; // 攻略文章内容，不需要在这里封装，用到的时候，在mapper里封装

    // 状态要翻译
    public String getStateName(){
        String msg = "草稿";
        if(state == STATE_RELEASE){
            msg = "发布";
        }
        return msg;
    }

    // 封装json数据
    public String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("title",title);
        map.put("sequence",sequence);
        map.put("coverUrl",coverUrl);
        map.put("state",state);
        if(catalog != null){
            map.put("strategyId",catalog.getStrategyId());
            map.put("catalogId",catalog.getId());
        }
        return JSONUtil.toJSONString(map);
    }

}