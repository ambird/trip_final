package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.NewsContent;

import java.util.List;

public interface NewsContentMapper {
    int insert(NewsContent record);

    List<NewsContent> selectAll();

    int updateByPrimaryKey(NewsContent content);

    NewsContent getContent(Long id);

    NewsContent selectByPrimaryKey(long id);

}