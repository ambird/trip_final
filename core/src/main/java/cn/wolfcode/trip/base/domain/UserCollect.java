package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class UserCollect extends BaseDomain{
    private Long collectId;//收藏内容id

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;//收藏时间

    private User user;//收藏用户

    private News news;//日报

    private Hotel hotel;//酒店

    private Strategy strategy;//攻略

    private Travel travel;//游记


}