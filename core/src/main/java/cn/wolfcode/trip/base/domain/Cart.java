package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cart extends BaseDomain{

    private User user; // 用户id

    private Product product; // 商品id

    private Integer count; // 商品数量

    private Long totalPrice; // 商品总价
}