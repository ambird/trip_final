package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.mapper.TravelCommendMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import cn.wolfcode.trip.base.util.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TravelCommendServiceImpl implements ITravelCommendService {

    @Autowired
    private TravelCommendMapper travelCommendMapper;

    public PageInfo<TravelCommend> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<TravelCommend> list = travelCommendMapper.selectForList(qo);
        return new PageInfo<>(list);
    }

    public void saveOrUpdate(TravelCommend entity) {
        // 专门处理攻略推荐
        if(entity.getType() == TravelCommend.TYPE_STRATEGY){
            // 1.删除所有的攻略推案
            travelCommendMapper.deleteAllStrategyCommand(TravelCommend.TYPE_STRATEGY);
            // 2.保存当前的攻略推荐
            travelCommendMapper.insert(entity);
            return;
        }
        if(entity.getId() == null){
            travelCommendMapper.insert(entity);
        } else {
            travelCommendMapper.updateByPrimaryKey(entity);
        }
    }

    public List<TravelCommend> listCommend(Integer type) {
        Date beginDate = null;
        Date endDate = new Date();
        // 设置好时间
        if(type == TravelCommend.TYPE_WEEK){
            // 设置当前周
            beginDate = DateUtil.getSevenDaysAgo(endDate);
        } else if(type == TravelCommend.TYPE_MONTH){
            // 设置好当前月
            beginDate = DateUtil.getAMonthAgo(endDate);
        }
        return travelCommendMapper.selectCommentByType(type, beginDate, endDate);
    }


}
