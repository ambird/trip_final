package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NewsQueryObject extends QueryObject{
    private Integer state;//日报的状态
}
