package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyVisit;
import cn.wolfcode.trip.base.domain.TravelVisit;

public interface IStrategyVisitService {
    void insert(StrategyVisit visit);

    void update(StrategyVisit visit);

    StrategyVisit get(Long travelId);

}
