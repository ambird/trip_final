package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface TravelCommendMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TravelCommend record);

    TravelCommend selectByPrimaryKey(Long id);

    List<TravelCommend> selectForList(QueryObject qo);

    int updateByPrimaryKey(TravelCommend record);

    List<TravelCommend> selectCommentByType(@Param("type") Integer type,
                                      @Param("beginDate") Date beginDate,
                                      @Param("endDate") Date endDate);

    void deleteAllStrategyCommand(Integer typeStrategy);
}