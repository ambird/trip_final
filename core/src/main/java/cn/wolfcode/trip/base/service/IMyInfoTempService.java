package cn.wolfcode.trip.base.service;

public interface IMyInfoTempService {
    /**
     * 清空用户我的信息临时信息
     * @param id 用户id
     * @param msgType 信息类型
     */
    void emptyTempMsg(Long id, String msgType);
}
