package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.MyInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MyInfoMapper {
    int insert(MyInfo record);

    List<MyInfo> selectAll();

    MyInfo selectInfoByUserId(Long id);

    void emptySystemMsgInfoByUserId(Long id);

    MyInfo selectCount(@Param("type") String type, @Param("id") Long id);

    void updateInfo(@Param("id") Long id, @Param("goodNum") Integer goodNum, @Param("type") String type);

    void emptyUserAddMsg(Long id);

    void updateAllUserSystemMsg();

}