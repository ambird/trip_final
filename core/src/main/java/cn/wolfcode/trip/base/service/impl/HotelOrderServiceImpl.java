package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.HotelOrder;
import cn.wolfcode.trip.base.mapper.HotelOrderMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IHotelOrderService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelOrderServiceImpl implements IHotelOrderService {

    //注入Auto对象

    @Autowired
    private HotelOrderMapper hotelOrderMapper;


    //这是分页的数据
    @Override
    public PageInfo<HotelOrder> query(QueryObject qo) {

        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());

        return new PageInfo<>(hotelOrderMapper.selectForListInHotelOrder(qo));
    }


    //保存或者更新操作
    @Override
    public void saveOrUpdate(HotelOrder entity) {
        if (entity.getId() == null) {
            hotelOrderMapper.insert(entity);
        }
        hotelOrderMapper.updateByPrimaryKey(entity);
    }
}
