package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Friends;
import cn.wolfcode.trip.base.domain.PersonalInterface;
import cn.wolfcode.trip.base.mapper.FriendsMapper;
import cn.wolfcode.trip.base.mapper.PersonalInterfaceMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IFriendsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FriendsServiceImpl implements IFriendsService {

    @Autowired
    private FriendsMapper friendsMapper;
    @Autowired
    private PersonalInterfaceMapper personalInterfaceMapper;

    public PageInfo<Friends> selectFans(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Friends> list = friendsMapper.selectFans(qo);
        return new PageInfo<>(list);
    }

    public PageInfo<Friends> selectFollow(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Friends> list = friendsMapper.selectFollow(qo);
        return new PageInfo<>(list);
    }

    public Friends getfansOrFollow(Long userId, Long toUserId) {
        Friends friends = friendsMapper.selectFollowOrFans(userId, toUserId);
        if (friends == null) {
            throw new RuntimeException("xxx");
        }
        return friends;
    }

    public void deleteFansOrFollow(Long userId, Long toUserId) {
        friendsMapper.deleteByPrimaryKey(userId, toUserId);

        PersonalInterface user = personalInterfaceMapper.selectByPrimaryKey(userId);
        user.setFollow(user.getFollow()-1);
        personalInterfaceMapper.updateByPrimaryKey(user);

        PersonalInterface toUser = personalInterfaceMapper.selectByPrimaryKey(toUserId);
        toUser.setFans(toUser.getFans()-1);
        personalInterfaceMapper.updateByPrimaryKey(toUser);
    }
}
