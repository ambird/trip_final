package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.HotelOrder;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IHotelOrderService {

    //查询客户订单的service
    PageInfo<HotelOrder> query(QueryObject qo);

    void saveOrUpdate(HotelOrder entity);
}
