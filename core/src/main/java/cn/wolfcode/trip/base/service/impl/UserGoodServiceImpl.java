package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.MyInfo;
import cn.wolfcode.trip.base.domain.MyInfoTemp;
import cn.wolfcode.trip.base.domain.UserGood;
import cn.wolfcode.trip.base.mapper.MyInfoMapper;
import cn.wolfcode.trip.base.mapper.MyInfoTempMapper;
import cn.wolfcode.trip.base.mapper.UserGoodMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IUserGoodService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserGoodServiceImpl implements IUserGoodService {
    @Autowired
    private UserGoodMapper userGoodMapper;
    @Autowired
    private MyInfoMapper infoMapper;
    @Autowired
    private MyInfoTempMapper infoTempMapper;

    public Long autoUserGoodId() {
        UserGood good = new UserGood();
        userGoodMapper.autoUserGoodId(good);
        return good.getId();
    }

    public void addGood(UserGood userGood) {
        Date date = new Date();
        // 点赞内容id
        userGood.setGoodId(userGood.getGoodId());
        userGood.setTime(date);
        userGoodMapper.insert(userGood);

        // 我的信息-点赞消息+1
        MyInfo info = infoMapper.selectCount("good", userGood.getUser().getId());
        infoMapper.updateInfo(userGood.getUser().getId(), info.getGood() + 1, "good");

        // 我的信息临时表-点赞消息+1
        MyInfoTemp infoTemp = infoTempMapper.selectCount("good", userGood.getUser().getId());
        infoTempMapper.updataInfo(userGood.getUser().getId(), infoTemp.getGood() + 1, "good");

    }

    public UserGood checkAddGood(Long userId, Long goodId) {
        UserGood userGood = userGoodMapper.selectGoodMsgByUserIdAndGoodId(userId, goodId);
        if (userGood != null) {
            throw new RuntimeException("已点赞");
        }
        return userGood;
    }

    public void removeGood(UserGood userGood) {
        // 取消赞内容id
        userGood.setGoodId(userGood.getGoodId());
        userGoodMapper.deleteGood(userGood);

        // 我的信息-点赞信息-1
        MyInfo info = infoMapper.selectCount("good", userGood.getUser().getId());
        if (info.getGood() == 0) {
            return;
        } else {
            infoMapper.updateInfo(userGood.getUser().getId(), info.getGood() - 1, "good");
        }
        // 我的信息临时表-点赞消息-1
        MyInfoTemp infoTemp = infoTempMapper.selectCount("good", userGood.getUser().getId());
        if (infoTemp.getGood() == 0) {
            return;
        } else {
            infoTempMapper.updataInfo(userGood.getUser().getId(), infoTemp.getGood() - 1, "good");
        }
    }

    public int getCount(Long goodId) {
        return userGoodMapper.selectCountGood(goodId);
    }

    public PageInfo<UserGood> userAddGoodMsg(Long id, QueryObject qo) {
        // 查出个人临时表中的点赞数量
        MyInfoTemp infoTemp = infoTempMapper.selectCount("good", id);
        // 当点赞数量不为0时查询所有的临时点赞数
        if (infoTemp.getGood() != 0) {
            if (infoTemp.getGood() > 20) {
                PageHelper.startPage(qo.getCurrentPage(), 20, qo.getOrderBy());
            } else {
                PageHelper.startPage(qo.getCurrentPage(), infoTemp.getGood(), qo.getOrderBy());
            }
            // 获取点赞信息
            List<UserGood> list = userGoodMapper.selectGoodMsgByUserId(id);
            if (list.size() > 0) {
                for (UserGood userGood : list) {
                    if (userGood.getTravel() != null) {
                        userGood.setAllAddGoodMsg(userGood.getTravel());
                    } else if (userGood.getStrategy() != null) {
                        userGood.setAllAddGoodMsg(userGood.getStrategy());
                    } else if (userGood.getNews() != null) {
                        userGood.setAllAddGoodMsg(userGood.getNews());
                    } else if (userGood.getHotel() != null) {
                        userGood.setAllAddGoodMsg(userGood.getHotel());
                    }
                }
            }
            // 清除个人信息界面点赞信息
            infoMapper.emptyUserAddMsg(id);
            return new PageInfo<>(list);
        }
        return null;
    }
}
