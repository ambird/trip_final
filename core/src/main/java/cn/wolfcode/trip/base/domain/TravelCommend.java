package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.DateUtil;
import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class TravelCommend extends BaseDomain{
    public static final Integer TYPE_WEEK = 1; //每周推荐
    public static final Integer TYPE_MONTH = 2; //每月推荐
    public static final Integer TYPE_STRATEGY = 3; //攻略推荐

    private Long travelId; // 推荐的游记id

    private String title;  // 包装的标题

    private String subTitle; // 包装的副标题

    private String coverUrl; // 包装的封面

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date schedule; // 推荐日期

    private Integer type = TYPE_WEEK; // 推荐类型

    // 根据实际需求增加的
    private User author;
    private String placeName;


    public String getTypeName(){
        String msg = "每周推荐";
        if (type == TYPE_MONTH) {
            msg = "每月推荐";
        } else if (type == TYPE_STRATEGY) {
            msg = "攻略推荐";
        }
        return msg;
    }

    public String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("travelId",travelId);
        map.put("title",title);
        map.put("subTitle",subTitle);
        map.put("coverUrl",coverUrl);
        map.put("schedule", DateUtil.formatDate(schedule));
        map.put("type",type);
        return JSONUtil.toJSONString(map);
    }
}