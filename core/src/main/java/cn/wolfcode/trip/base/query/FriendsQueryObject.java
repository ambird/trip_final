package cn.wolfcode.trip.base.query;

import lombok.Data;

@Data
public class FriendsQueryObject extends QueryObject {
    private Long userId;
    private Long toUserId;
}
