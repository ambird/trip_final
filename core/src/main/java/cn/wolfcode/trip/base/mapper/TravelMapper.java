package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface TravelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Travel record);

    Travel selectByPrimaryKey(Long id);

    List<Travel> selectForList(QueryObject qo);

    int updateByPrimaryKey(Travel record);

    // 更新游记状态
    void updateState(@Param("id") Long id,
                     @Param("state") Integer state,
                     @Param("releaseTime") Date releaseTime);

    List<TravelVisit> selectVisits();

    Integer getTravelCount(Long authorId);
}