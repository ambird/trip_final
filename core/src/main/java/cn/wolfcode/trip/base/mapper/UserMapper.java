package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    User selectByPrimaryKey(Long id);

    List<User> selectAll();

    int updateByPrimaryKey(User record);

    User checkByEmail(String email);

    User selectByInfo(@Param("email")String email, @Param("password")String password);

    List<User> selectForList(QueryObject qo);

    // 根据id更新用户签到积分
    void updateConDaysById(@Param("id")Long id, @Param("integral")Long integral);

    User selectInterface(Long id);

    /**
     * 根据邮箱查询用户的id
     * @param email
     * @return
     */
    Long selectIdByEmail(String email);

    // 用户购买商品后，更新积分
    void updateInteralByPrice(@Param("id") Long id, @Param("integral") Long integral);
}