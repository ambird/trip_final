package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.query.ProductQueryObject;
import com.github.pagehelper.PageInfo;

public interface IProductService {
    // 查询商品
    PageInfo<Product> queryProduct(ProductQueryObject qo);

    // 更新、增加商品
    void saveOrUpdate(Product entity);

    //根据id查询商品内容
    String getContent(Long id);

    // 根据商品的id查询商品
    Product get(long id);

    // 上架商品
    void changeState(Long id, Integer state);


}
