package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 系统信息
 */
@Getter
@Setter
public class SystemMsg extends BaseDomain {
    public static final Integer STATE_PUBLIC = 0; //全部人
    public static final Integer STATE_PRIVATE = 1; //个人

    private String content; // 信息内容

    private String title;   // 标题

    private Integer state = STATE_PUBLIC;   // 状态

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    private Long userId;

    //冗余字段
    private User user;

    public String getStateName(){
        String msg = "全部人";
        if (state == STATE_PRIVATE){
            msg = "个人";
        }
        return msg;
    }
}