package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.News;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface NewsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(News record);

    News selectByPrimaryKey(Long id);

    List<News> selectForList(QueryObject qo);

    int updateByPrimaryKey(News record);

    /**
     * 修改游记的状态
     * @param id
     * @param state
     */
    void updateState(@Param("id") Long id, @Param("state") Integer state, @Param("releaseTime") Date releaseTime);


    /**
     * 修改之前的头条状态
     * @param state
     */
    void updateOldState(Integer state);
}