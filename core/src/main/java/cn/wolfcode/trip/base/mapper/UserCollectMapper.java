package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.domain.UserCollect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserCollectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserCollect record);

    UserCollect selectByPrimaryKey(Integer id);

    List<UserCollect> selectAll();

    int updateByPrimaryKey(UserCollect record);

    void autoUserCollectId(User user);

    UserCollect selectCollectMsgByUserIdAndCollectId(@Param("userId") Long userId, @Param("collectId") Long collectId);

    void deleteCollect(UserCollect userCollect);

    int selectCountCollect(Long collectId);

    List<UserCollect> getCollectsByType(@Param("userId") Long userId, @Param("type") String type);

    int getCollectsCountByType(@Param("userId") Long userId, @Param("type") String type);
}