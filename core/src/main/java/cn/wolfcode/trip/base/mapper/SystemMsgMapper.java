package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.SystemMsg;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemMsgMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemMsg record);

    SystemMsg selectByPrimaryKey(Long id);

    List<SystemMsg> selectAll(QueryObject qo);

    int updateByPrimaryKey(SystemMsg record);

    List<SystemMsg> getUserSystemMsg(@Param("id") Long id);

    String selectContentById(Long id);

}