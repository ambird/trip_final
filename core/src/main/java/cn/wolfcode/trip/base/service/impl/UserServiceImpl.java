package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.*;
import cn.wolfcode.trip.base.mapper.*;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PersonalInterfaceMapper personalInterfaceMapper;
    @Autowired
    private FriendsMapper friendsMapper;
    @Autowired
    private AttendanceMapper attendanceMapper;
    @Autowired
    private MyInfoMapper infoMapper;
    @Autowired
    private MyInfoTempMapper tempMapper;

    public void regist(User u) {
        // 先判断是否存在该用户
        User user = userMapper.checkByEmail(u.getEmail());
        if(user != null){
            throw new RuntimeException("用户名已经存在哦");
        }
        saveOrUpdate(u);
    }

    public void saveOrUpdate(User u) {
        // 用户注册
        if(u.getId() == null){
            u.setCoverImgUrl("/img/user/bg.jpeg");
            u.setHeadImgUrl("/img/user/head.jpg");
            u.setGender(User.GENDER_NONE);
            u.setPlace("gz");
            u.setSign("hello,world");
            u.setIntegral(5L);
            userMapper.insert(u);
            // 新用户创建时默认签到一天
            Attendance attendance = new Attendance();
            attendance.setUser_id(u.getId());
            attendance.setSignInTime(new Date());
            attendance.setConDays(1);
            attendanceMapper.insert(attendance);
            // 新用户创建默认粉丝和关注为0
            PersonalInterface personalInterface = new PersonalInterface();
            personalInterface.setId(u.getId());
            personalInterface.setFans(0);
            personalInterface.setFollow(0);
            personalInterfaceMapper.insert(personalInterface);
            // 创建个人信息
            MyInfo info = new MyInfo();
            info.setId(u.getId());
            infoMapper.insert(info);

            // 创建临时个人信息
            MyInfoTemp infoTemp = new MyInfoTemp();
            infoTemp.setId(u.getId());
            tempMapper.insert(infoTemp);
        } else {
            // 修改该用户
            userMapper.updateByPrimaryKey(u);
        }
    }

    // 用户登陆
    public User login(String email, String password) {
        User user = userMapper.selectByInfo(email, password);
        if(user == null){
            throw new RuntimeException("用户名或密码错误");
        }
        return user;
    }

    public PageInfo<User> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<User> list = userMapper.selectForList(qo);
        return new PageInfo<>(list);
    }

    public void updateIntegral(Long id, Long integral) {
        userMapper.updateConDaysById(id,integral);
    }



    public User selectInterface(Long id) {
        return userMapper.selectInterface(id);
    }

    public User getUser(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }

    // 点击关注
    public void fansOrFollow(Long userId, Long toUserId) {
        // 关注人的关注数 + 1
        PersonalInterface user = personalInterfaceMapper.selectByPrimaryKey(userId);
        user.setFollow(user.getFollow() + 1);
        personalInterfaceMapper.updateByPrimaryKey(user);

        // 被关注人的粉丝数 + 1
        PersonalInterface toUser = personalInterfaceMapper.selectByPrimaryKey(toUserId);
        toUser.setFans(toUser.getFans() + 1);
        personalInterfaceMapper.updateByPrimaryKey(toUser);

        // 新增谁关注了谁
        Friends friends = new Friends();
        friends.setUserId(userId);
        friends.setToUserId(toUserId);
        friends.setFollowTime(new Date());
        friendsMapper.insert(friends);
    }

    // 用户购买商品后，更新用户积分
    public void updateIntegralByPrice(Long id, Long integral) {
        userMapper.updateInteralByPrice(id, integral);
    }


    public List<User> selectAllUsers() {
        return userMapper.selectAll();
    }
}
