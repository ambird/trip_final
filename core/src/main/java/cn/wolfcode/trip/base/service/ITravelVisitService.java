package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.TravelVisit;

import java.util.List;

public interface ITravelVisitService {
    void insert(TravelVisit visit);

    void update(TravelVisit visit);

    TravelVisit get(Long travelId);

}
