package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyInfoTemp extends BaseDomain{
    private Integer good = 0;

    private Integer comment = 0;

    private Integer atteme = 0;

    private Integer findme = 0;

    private Integer tellme = 0;
}