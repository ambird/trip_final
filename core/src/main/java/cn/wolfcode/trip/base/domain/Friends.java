package cn.wolfcode.trip.base.domain;

import lombok.Data;

import java.util.Date;

@Data
public class Friends extends BaseDomain {
    private Long userId;

    private Long toUserId;

    private User user;

    private Date followTime;
}