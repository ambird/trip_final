package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 我的消息
 */
@Getter
@Setter
public class MyInfo extends BaseDomain{
    private Integer good = 0;   // 赞

    private Integer comment = 0;    // 评论

    private Integer aateme = 0; // @我

    private Integer findme = 0; // 私信

    private Integer tellme = 0; // 系统通知
}