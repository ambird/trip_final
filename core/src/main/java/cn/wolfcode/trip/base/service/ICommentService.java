package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Comment;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 评论服务接口
 */
public interface ICommentService{
    /**
     * 增加评论
     * @param comment
     */
    void saveComment(Comment comment);

    /**
     * 根据评论内容id查询相对于的评论
     * @param id 评论内容id
     * @return 评论列表
     */
    PageInfo<Comment> queryCommentById(Long id, QueryObject qo);

    /**
     * 自动生成commentId
     * @return
     */
    Long autoCommentId();

    /**
     * 根据传入的用户的id和哪个类型的评论查找该用户在该类型下的评论
     * @param userId
     * @param type
     * @return
     */
    List<Comment> getUserCommentByType(Long userId, String type);

    /**
     * 查询某个类型的评论总数
     * @param userId
     * @param type
     * @return
     */
    int getUserCommentsCountByType(Long userId, String type);

    PageInfo<Comment> selectCommentAllByUserId(Long userId, QueryObject qo);

    Integer getCommentCountByUserId(Long userId);



    /**
     * 查看用户评论信息
     * @param id 用户id
     * @param qo
     * @return
     */
    PageInfo<Comment> userCommentMsg(Long id, QueryObject qo);

}
