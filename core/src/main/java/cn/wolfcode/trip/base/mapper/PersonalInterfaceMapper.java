package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.PersonalInterface;
import java.util.List;

public interface PersonalInterfaceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PersonalInterface record);

    PersonalInterface selectByPrimaryKey(Long id);

    List<PersonalInterface> selectAll();

    int updateByPrimaryKey(PersonalInterface record);
}