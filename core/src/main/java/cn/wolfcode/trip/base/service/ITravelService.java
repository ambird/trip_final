package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.query.TravelQueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ITravelService {

   PageInfo<Travel> queryTravel(TravelQueryObject qo);

   // 更新、增加游记
   void saveOrUpdate(Travel entity);

    // 根据游记的id查询游记
    Travel get(long id);

    // 获得游记正文内容
    String getContent(Long id);

    // 提交、拒绝游记
    void changeState(Long id, Integer state);

    List<TravelVisit> selectVisits();

    Integer getTravelCountById(Long authorId);
}
