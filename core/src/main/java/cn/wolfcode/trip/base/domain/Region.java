package cn.wolfcode.trip.base.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Region extends BaseDomain{

    public static final Integer STATE_NORMAL = 0;  // 普通
    public static final Integer STATE_COMMEND = 1; // 推荐

    private String name; // 区域名称

    private Integer state = STATE_NORMAL; // 状态

    private Region parent; //  父区域

    // 封装成treeView想要的格式
    // {text:xx, tags:[xx], id:xx}
    public Map<String,Object> toTreeView() {
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        map.put("text",name);
        map.put("lazyLoad",true);
        if(state == STATE_COMMEND){
            map.put("tags",new String[]{"推荐"});
        }

        // 到时候回显用
        if(parent != null){
            map.put("parentId",parent.getId());
            map.put("parentName",parent.getName());
        }
        return map;

    }
}