package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
public class QueryObject {
    private int currentPage = 1;
    private int pageSize = 3;
    // 关键字
    private String keyword;

    // 顺序
    private String orderBy;
    public int getStartIndex(){
        return (currentPage - 1) * pageSize;
    }

    public String empty2Null(String str){
        return StringUtils.hasLength(str) ? str : null;
    }

    public String getKeyword(){
        return empty2Null(keyword);
    }
}
