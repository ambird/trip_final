package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IUserService {


    void regist(User u);

    void saveOrUpdate(User u);

    User login(String email, String password);

    PageInfo<User> query(QueryObject qo);

    /**
     * 签到后，更新用户积分
     * @param id
     * @param integral
     */
    void updateIntegral(Long id, Long integral);

    User selectInterface(Long id);

    User getUser(Long id);

    void fansOrFollow(Long userId, Long toUserId);

    /**
     * 购买商品后，更新用户积分
     * @param id
     * @param balance
     */
    void updateIntegralByPrice(Long id, Long balance);


    List<User> selectAllUsers();
}
