package cn.wolfcode.trip.base.filter;

import cn.wolfcode.trip.base.util.UploadUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

// 图片过滤器，访问/upload的资源被转换为访问本地磁盘上的upload文件夹
@WebFilter("/upload/*")
public class ImageFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String uri = req.getRequestURI();
        String filename = uri.substring(uri.lastIndexOf('/') + 1);

        Path path = Paths.get(UploadUtil.imgDir, filename);
        if (Files.exists(path)) {
            resp.setContentType("image");
            Files.copy(path, response.getOutputStream());
        } else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException {}

    public void destroy() {}
}
