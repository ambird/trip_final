package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
// 打卡类
@Getter
@Setter
public class Attendance extends BaseDomain{

    private Date signInTime; // 最近一次打卡时间

    private Integer conDays; // 连续打卡天数

    private long user_id; // 打卡用户

    private long integral; // 冗余字段，user积分

    private String msg; // 提醒信息
}