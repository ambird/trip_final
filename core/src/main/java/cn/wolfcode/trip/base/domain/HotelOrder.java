package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.DateUtil;
import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class HotelOrder extends BaseDomain {

    public static final Integer GENDER_NONE = -1; //保密
    public static final Integer GENDER_MALE = 1; //男
    public static final Integer GENDER_FEMALE = 0; //女

    private String customerName;

    private Integer tel;

    private BigDecimal pubPrice;

    private Integer gender = GENDER_NONE;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date liveTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date outTime;

    private Region region;

    private Hotel hotel;


    //现在对性别在admin端做回显功能
    public String getGenderName() {
        String temp = null;
        if (gender == GENDER_NONE) {
            temp = "保密";
        } else if (gender == GENDER_MALE) {
            temp = "男";
        } else if (gender == GENDER_FEMALE) {
            temp = "女";
        }
        return temp;
    }


    //用来回显相关的数据
    public String getJson() {
        Map<String, Object> map = new HashMap<>();

        map.put("customerName", customerName);
        map.put("id", id);
        map.put("tel", tel);
        map.put("pubPrice", hotel.getPubPrice());
        map.put("gender", gender);
        map.put("liveTime", DateUtil.formatDate(liveTime));
        map.put("outTime", DateUtil.formatDate(outTime));
        if (region != null) {

            map.put("regionId", region.getId());
        }
        if (hotel != null) {

            map.put("hotelId", hotel.getId());
        }
        return JSONUtil.toJSONString(map);
    }
}