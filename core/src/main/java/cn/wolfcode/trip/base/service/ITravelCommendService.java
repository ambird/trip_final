package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ITravelCommendService {

    PageInfo<TravelCommend> query(QueryObject qo);

    void saveOrUpdate(TravelCommend entity);

    List<TravelCommend> listCommend(Integer type);
}
