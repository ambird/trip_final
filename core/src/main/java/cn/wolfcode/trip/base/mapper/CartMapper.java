package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Cart;
import java.util.List;

public interface CartMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Cart record);

    Cart selectByPrimaryKey(Long id);

    List<Cart> selectAll();

    int updateByPrimaryKey(Cart record);
}