package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties("handler")
public class Hotel extends BaseDomain {

    public static final Integer STATE_NOMAL = 1; //普通
    public static final Integer STATE_COMMEND = 2; //推荐
    public static final Integer STATE_STRONG = 3; //推荐


    private String perExpends;
    private String name;//酒店名称

    private String title;//酒店标题

    private String coverUrl;//酒店封面

    private Long pubPrice;//酒店标准价

    private Long vipPrice;//酒店会员价

    private Integer state = STATE_NOMAL;//酒店装填

    private Integer good;//点赞数

    private Region place;//所在区域

    private Long commentId; // 所属评论id

    private Long goodId; // 所属点赞id

    private Long collectId; // 所属收藏id


    private HotelContent hotelContent;

    private User user;//封装用户的个人信息

    // 封装转化state为文字，以用来回显
    public String getStateName() {
        String msg = null;
        if (state == STATE_NOMAL) {
            msg = "普通";
        } else if (state == STATE_COMMEND) {
            msg = "推荐";
        } else if (state == STATE_STRONG) {
            msg = "强烈推荐";
        }
        return msg;
    }


    //用来回显相关的数据
    public String getJson() {
        Map<String, Object> map = new HashMap<>();

        map.put("perExpends", perExpends);
        map.put("id", id);
        map.put("name", name);
        map.put("title", title);
        map.put("coverUrl", coverUrl);
        map.put("pubPrice", pubPrice);
        map.put("vipPrice", vipPrice);
        map.put("state", getStateName());
        if (place != null) {

            map.put("placeId", place.getId());
        }
        return JSONUtil.toJSONString(map);
    }

}