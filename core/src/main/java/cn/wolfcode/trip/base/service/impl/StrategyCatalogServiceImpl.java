package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.mapper.StrategyCatalogMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StrategyCatalogServiceImpl implements IStrategyCatalogService {

    @Autowired
    private StrategyCatalogMapper strategyCatalogMapper;



    public void saveOrUpdate(StrategyCatalog entity) {

        if(entity.getSequence() == null){
            // 用户没有填序号，可以自动生成
            // 先查询当前攻略中最大序号
            Integer sequence = strategyCatalogMapper
                    .selectMaxSequenceByStrategyId(entity.getStrategyId());
            // 增加
            if(sequence == null){
                sequence = 0;
            }
            entity.setSequence(sequence + 1);
        }

        if (entity.getId() == null) {
            strategyCatalogMapper.insert(entity);
        } else {
            // 修改
            strategyCatalogMapper.updateByPrimaryKey(entity);
        }
    }

    public PageInfo<StrategyCatalog> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize());
        List<StrategyCatalog> list = strategyCatalogMapper.selectForList(qo);
        return new PageInfo<>(list);

    }

    public List<StrategyCatalog> list(Long strategyId) {
        return strategyCatalogMapper.selectEnableCatalog(strategyId);
    }

}
