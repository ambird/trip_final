package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.MyInfoTemp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MyInfoTempMapper {
    int insert(MyInfoTemp record);

    List<MyInfoTemp> selectAll();

    MyInfoTemp selectCount(@Param("type") String type, @Param("id") Long id);

    void updataInfo(@Param("id") Long id, @Param("typeNum") int typeNum, @Param("type") String type);

    void updateTempMsg(@Param("id") Long id, @Param("msgType") String msgType);

    void updateAllUserSystemMsg();

}