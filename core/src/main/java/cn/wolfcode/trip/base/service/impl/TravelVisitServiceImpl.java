package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.TravelVisit;
import cn.wolfcode.trip.base.mapper.TravelVisitMapper;
import cn.wolfcode.trip.base.service.ITravelVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TravelVisitServiceImpl implements ITravelVisitService {
    @Autowired
    private TravelVisitMapper visitMapper;

    @Override
    public void insert(TravelVisit visit) {
        visitMapper.insert(visit);
    }

    @Override
    public void update(TravelVisit visit) {
        visitMapper.updateByPrimaryKey(visit);
    }

    @Override
    public TravelVisit get(Long travelId) {
        return visitMapper.selectByPrimaryKey(travelId);
    }

}
