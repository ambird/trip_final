package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Product record);

    Product selectByPrimaryKey(Long id);

    String selectContentById(Long id);

    List<Product> selectAll(QueryObject qo);

    int updateByPrimaryKey(Product record);

    // 更新商品状态
    void updateState(@Param("id") Long id,@Param("state") Integer state);

}