package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.mapper.RegionMapper;
import cn.wolfcode.trip.base.service.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RegionServiceImpl implements IRegionService {
    @Autowired
    private RegionMapper regionMapper;

    public List<Region> listMenus(Long parentId) {
       return regionMapper.selectMenus(parentId);
    }

    public void saveOrUpdate(Region entity) {
        if(entity.getId() == null){
            regionMapper.insert(entity);
        } else {
            regionMapper.updateByPrimaryKey(entity);
        }
    }

    public List<Region> list(Integer state) {
        return regionMapper.selectAll(state);
    }
}
