package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Region;

import java.util.List;

public interface IRegionService {

    /**
     * 根据父区域prentId查询区域对象，如果没有传递parentId,则查询所有根区域
     * @param parentId
     * @return
     */
    List<Region> listMenus(Long parentId);

    void saveOrUpdate(Region entity);


    List<Region> list(Integer state);

}
