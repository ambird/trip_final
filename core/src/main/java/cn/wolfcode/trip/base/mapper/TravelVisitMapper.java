package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.TravelVisit;
import java.util.List;

public interface TravelVisitMapper {

    int insert(TravelVisit record);

    TravelVisit selectByPrimaryKey(Long id);

    List<TravelVisit> selectAll();

    int updateByPrimaryKey(TravelVisit record);
}