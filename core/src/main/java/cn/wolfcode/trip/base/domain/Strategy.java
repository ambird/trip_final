package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Strategy extends BaseDomain{

    public static final Integer STATE_NOMAL = 0; //正常
    public static final Integer STATE_DISABLE = 1; //禁用
    public static final Integer STATE_COMMEND = 2; //推荐

    private Region place;  //所属地区

    private String title; //攻略标题

    private String subTitle; //攻略副标题

    private String coverUrl; //攻略封面

    private Integer state = STATE_NOMAL; //攻略状态

    private Long commentId; // 所属评论id

    private Long goodId; // 所属点赞id

    private Long collectId; // 所属收藏id

    private int visit; // 访问量

    private int goods; // 点赞数量

    private int collects; // 收藏量

    // 翻译state
    public String getStateName(){
        String msg = "正常";
        if(state == STATE_DISABLE){
            msg = "禁用";
        } else if(state == STATE_COMMEND){
            msg = "推荐";
        }
        return msg;
    }

    // 封装json数据
    public String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("title",title);
        map.put("subTitle",subTitle);
        map.put("state",state);
        map.put("coverUrl",coverUrl);
        if(place != null){
            map.put("placeId",place.getId());
        }
        return JSONUtil.toJSONString(map);
    }
}