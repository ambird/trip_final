package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.StrategyContent;

public interface StrategyContentMapper {

    int insert(StrategyContent record);

    StrategyContent selectByPrimaryKey(Long id);


    int updateByPrimaryKey(StrategyContent record);
}