package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.MyInfo;
import cn.wolfcode.trip.base.domain.MyInfoTemp;
import cn.wolfcode.trip.base.domain.SystemMsg;
import cn.wolfcode.trip.base.mapper.MyInfoMapper;
import cn.wolfcode.trip.base.mapper.MyInfoTempMapper;
import cn.wolfcode.trip.base.mapper.SystemMsgMapper;
import cn.wolfcode.trip.base.mapper.UserMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.ISystemMsgService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SystemMsgServiceImpl implements ISystemMsgService {
    @Autowired
    private SystemMsgMapper systemMsgMapper;
    @Autowired
    private MyInfoMapper infoMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MyInfoTempMapper infoTempMapper;

    public PageInfo<SystemMsg> selectUserSystemMsg(QueryObject qo, Long id) {
        // 查出个人临时表中的点赞数量
        MyInfoTemp infoTemp = infoTempMapper.selectCount("tellme", id);
        // 当点赞数量不为0时查询所有的临时点赞数
        if (infoTemp.getTellme() != 0) {
            if (infoTemp.getTellme() > 20) {
                PageHelper.startPage(qo.getCurrentPage(), 20, qo.getOrderBy());
            } else {
                PageHelper.startPage(qo.getCurrentPage(), infoTemp.getTellme(), qo.getOrderBy());
            }
            // 获取临时系统消息
            List<SystemMsg> list = systemMsgMapper.getUserSystemMsg(id);
            // 清空系统通知信息
            infoMapper.emptySystemMsgInfoByUserId(id);
            return new PageInfo<>(list);
        }
        return null;
    }

    public PageInfo<SystemMsg> query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<SystemMsg> list = systemMsgMapper.selectAll(qo);
        return new PageInfo<>(list);
    }

    /**
     * 根据id去查询内容
     * @param id
     * @return
     */
    public String selectContentById(Long id) {
        return systemMsgMapper.selectContentById(id);
    }

    /**
     * 保存发布的信息
     * @param entity
     */
    public void save(SystemMsg entity) {
        Long id = null;
        //设置发送时间
        entity.setTime(new Date());
        //发送给全部人
        if (entity.getState() == 0){
            systemMsgMapper.insert(entity);
        }else if (entity.getState() == 1){
            //发送给个人
            //根据传过来的邮箱查询用户id
            id = userMapper.selectIdByEmail(entity.getUser().getEmail());
            entity.setUserId(id);
            systemMsgMapper.insert(entity);
        }

        // 我的信息-系统消息+1
        if (id == null) {
            // 全部信息,所有人+1
            infoMapper.updateAllUserSystemMsg();

            infoTempMapper.updateAllUserSystemMsg();
        } else {
            // 个人+1
            MyInfo info = infoMapper.selectCount("tellme", id);
            infoMapper.updateInfo(id, info.getTellme() + 1, "tellme");

            // 我的信息临时表-系统消息+1
            MyInfoTemp infoTemp = infoTempMapper.selectCount("tellme", id);
            infoTempMapper.updataInfo(id, infoTemp.getTellme() + 1, "tellme");

        }



    }
}
