package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.MyInfo;
import cn.wolfcode.trip.base.mapper.MyInfoMapper;
import cn.wolfcode.trip.base.service.IMyInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyInfoServiceImpl implements IMyInfoService {
    @Autowired
    private MyInfoMapper myInfoMapper;

    public MyInfo getInfoByUserId(Long id) {
        return myInfoMapper.selectInfoByUserId(id);
    }
}
