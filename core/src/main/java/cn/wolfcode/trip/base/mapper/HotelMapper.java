package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.Hotel;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HotelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Hotel record);

    Hotel selectByPrimaryKey(Long id);

    List<Hotel> selectAll();

    int updateByPrimaryKey(Hotel record);

    List<Hotel> selectForList(@Param("state") Integer state);


    List<Hotel> selectInAdmin(QueryObject qo);

    List<Hotel> getHotelsByState(QueryObject qo);

}