package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Setter@Getter
public class Product extends BaseDomain{

    public static final Integer STATE_WAIT = 0; //未上架
    public static final Integer STATE_RELEASE = 1; //已上架

    private String coverUrl; // 商品图片

    private String name; // 商品名称

    private Long price; // 商品价格

    private Integer state = STATE_WAIT; // 默认商品未上架

    private String content; // 商品详情内容

    public String getStateName(){
        String msg = "未上架";
        if(state == STATE_RELEASE){
            msg = "已上架";
        }
        return msg;
    }

    public String getJson(){
        Map<String, Object> map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("price",price);
        map.put("state",state);
        map.put("content",content);
        map.put("coverUrl",coverUrl);
        return JSONUtil.toJSONString(map);
    }


}