package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

public interface IStrategyDetailService {

    void saveOrUpdate(StrategyDetail entity);

    PageInfo<StrategyDetail> query(QueryObject qo);

    /**
     * 根据id查询攻略文章的内容
     * @param id
     * @return
     */
    String getContentById(Long id);

    StrategyDetail get(Long id);

    /**
     * 根据类别的id去查询大攻略
     * @param catalogId
     * @return
     */
    int selectStragyByCatalogId(Long catalogId);
}
