package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Product;
import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.mapper.ProductMapper;
import cn.wolfcode.trip.base.query.ProductQueryObject;
import cn.wolfcode.trip.base.service.IProductService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    public PageInfo<Product> queryProduct(ProductQueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(), qo.getPageSize(), qo.getOrderBy());
        List<Product> list = productMapper.selectAll(qo);
        return new PageInfo<>(list);
    }

    // 新增、更新商品
    public void saveOrUpdate(Product entity) {
        if(entity.getId() == null){
            productMapper.insert(entity);
        } else {
            productMapper.updateByPrimaryKey(entity);
        }
    }


    //根据商品id查询商品内容
    @Override
    public String getContent(Long id) {
        return productMapper.selectContentById(id);
    }

    // 根据商品的id查询商品
    public Product get(long id) {
        return productMapper.selectByPrimaryKey(id);
    }

    // 提交、拒绝游记
    public void changeState(Long id, Integer state) {
        if(state == Product.STATE_RELEASE){
            // 拒绝操作
            productMapper.updateState(id, state);
        } else {
            // 发布操作
            productMapper.updateState(id, state);
        }
    }
}
