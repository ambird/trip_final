package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StrategyDetailMapper {

    int insert(StrategyDetail record);

    StrategyDetail selectByPrimaryKey(Long id);

    List<StrategyDetail> selectForList(QueryObject qo);

    int updateByPrimaryKey(StrategyDetail record);

    Integer selectMaxSequenceByCatalogId(Long catalogId);

    int selectStragyByCatalogId(@Param("catalogId") Long catalogId);
}