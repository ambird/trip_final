package cn.wolfcode.trip.base.mapper;

import cn.wolfcode.trip.base.domain.UserGood;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserGoodMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserGood record);

    UserGood selectByPrimaryKey(Long id);

    List<UserGood> selectAll();

    int updateByPrimaryKey(UserGood record);

    void autoUserGoodId(UserGood good);

    UserGood selectGoodMsgByUserIdAndGoodId(@Param("userId") Long userId, @Param("goodId") Long goodId);

    void deleteGood(UserGood userGood);

    int selectCountGood(Long goodId);

    List<UserGood> selectGoodMsgByUserId(@Param("id") Long id);
}